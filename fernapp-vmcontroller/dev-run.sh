#!/bin/sh

#
# Script to compile and start fernapp server for development purposes.
# Specify app to start as first parameter (otherwise gnome-terminal will be started)
#

killall -q Xvfb

set -e

BASE_DIR=`pwd`
(cd ../ && mvn install -DskipTests)
rm -rf $BASE_DIR/target/web
unzip -d $BASE_DIR/target/web $BASE_DIR/target/ext/fernapp-ua-swing-web.zip

JAVA_OPTS="-Djava.library.path=$BASE_DIR/target/ext -Dfernapp.webroot=$BASE_DIR/target/web -Xcheck:jni -ea"
if [ -z "$1" ]; then
	APP="/usr/bin/gnome-terminal"   # gedit /usr/bin/gnome-terminal /usr/bin/libreoffice ../libfernwm/gtkhello/gtkhello
else
	APP="$@"
fi
FERN_OPTS="-passphrase 123 -resources $BASE_DIR/dist $APP"

# enable core dumps for debugging
ulimit -c unlimited

SERVER_JAR="$BASE_DIR/target/fernapp-vmcontroller-jar-with-dependencies.jar"
mkdir -p $BASE_DIR/target/run
(cd $BASE_DIR/target/run && java $JAVA_OPTS -jar $SERVER_JAR $FERN_OPTS)
	
#if [ -z "$1" ]; then
#    echo "Starting with maven"
#    export MAVEN_OPTS="$JAVA_OPTS"
#	mvn -e compile exec:java -Dfernapp.webroot=$BASE_DIR/target/web -Dexec.args="$FERN_OPTS"
#else
#    echo "Starting without recompiling the java part"
#fi
