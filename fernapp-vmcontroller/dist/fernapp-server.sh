#!/bin/bash

killall -q Xvfb

# enable core dumps for debugging
ulimit -c unlimited

echo "Starting fernapp server"
JAVA_VERSION=`java -version 2>&1`
echo System: `uname -a`
echo Java version: $JAVA_VERSION

LINUX_ARCH=`uname -i`
if [[ "$LINUX_ARCH" == *"64"*  &&  "$JAVA_VERSION" != *"64-Bit"*  ]]; then
  echo; echo "You have a 64-bit system - please use a 64-bit Java Runtime Environment"
  exit
fi


export LD_LIBRARY_PATH=./lib
unzip -n -q -d web fernapp-ua-swing-web.zip
java -Djava.library.path=$LD_LIBRARY_PATH -Dfernapp.webroot=./web -ea -jar fernapp-vmcontroller-jar-with-dependencies.jar -passphrase 123 "$@"
