package com.fernapp.integration;

import java.awt.image.BufferedImage;


import com.fernapp.useragent.ui.InputEventListener;
import com.fernapp.useragent.ui.WindowContainer;
import com.fernapp.useragent.ui.WindowStructuralListener;
import com.google.common.base.Preconditions;


/**
 * @author Markus
 * 
 */
public class MockWindowContainer implements WindowContainer {

	private int contentWidth;
	private int contentHeight;
	private boolean visible;
	private InputEventListener inputEventListener;

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setContentSize(int, int)
	 */
	@Override
	public void setContentSize(int width, int height) {
		contentWidth = width;
		contentHeight = height;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentWidth()
	 */
	@Override
	public int getContentWidth() {
		return contentWidth;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentHeight()
	 */
	@Override
	public int getContentHeight() {
		return contentHeight;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setContentLocation(int, int)
	 */
	@Override
	public void setContentLocation(int x, int y) {
		// ignore
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentLocationLeft()
	 */
	@Override
	public int getContentLocationLeft() {
		return 10;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#getContentLocationTop()
	 */
	@Override
	public int getContentLocationTop() {
		return 10;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		// ignore
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#updateContent(java.awt.image.BufferedImage)
	 */
	@Override
	public void updateContent(BufferedImage bufferedImage) {
		// ignore
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#show()
	 */
	@Override
	public void show() {
		visible = true;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#isVisible()
	 */
	@Override
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#dispose()
	 */
	@Override
	public void dispose() {
		// ignore
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#addInputEventListener(com.fernapp.useragent.ui.InputEventListener)
	 */
	@Override
	public void addInputEventListener(InputEventListener iel) {
		Preconditions.checkState(this.inputEventListener == null);
		this.inputEventListener = iel;
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#addWindowStructuralListener(com.fernapp.useragent.ui.WindowStructuralListener)
	 */
	@Override
	public void addWindowStructuralListener(WindowStructuralListener contentResizeListener) {
		// ignore
	}

	/**
	 * @return the inputEventListener
	 */
	public InputEventListener getInputEventListener() {
		return inputEventListener;
	}

}
