package com.fernapp.vmcontroller.executor.X11;

import java.awt.event.KeyEvent;

import org.junit.Assert;
import org.junit.Test;

import com.fernapp.vmcontroller.executor.X11.KeycodeConverter;
import com.fernapp.vmcontroller.executor.X11.XKeySymConstants;


/**
 * 
 * http://docs.oracle.com/javase/tutorial/uiswing/events/keylistener.html
 * 
 * @author Markus
 * 
 */
public class KeycodeConverterTest {

	@Test
	public void conversions() {
		// g
		Assert.assertEquals(103, KeycodeConverter.getKeysym(71, 'g', KeyEvent.KEY_LOCATION_STANDARD));

		// m, M
		Assert.assertEquals(109, KeycodeConverter.getKeysym(77, 'm', KeyEvent.KEY_LOCATION_STANDARD));
		Assert.assertEquals(109, KeycodeConverter.getKeysym(77, 'M', KeyEvent.KEY_LOCATION_STANDARD));

		// 1
		Assert.assertEquals(XKeySymConstants.XK_1, KeycodeConverter.getKeysym(49, '1', KeyEvent.KEY_LOCATION_STANDARD));

		// Enter
		Assert.assertEquals(XKeySymConstants.XK_Return, KeycodeConverter.getKeysym(10, '\n', KeyEvent.KEY_LOCATION_STANDARD));

		// shift
		Assert.assertEquals(XKeySymConstants.XK_Shift_L, KeycodeConverter.getKeysym(16, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT));

		// Alt Gr
		Assert.assertEquals(XKeySymConstants.XK_ISO_Level3_Shift,
				KeycodeConverter.getKeysym(KeyEvent.VK_ALT_GRAPH, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD));

		// Euro
		Assert.assertEquals(XKeySymConstants.XK_EuroSign, KeycodeConverter.getKeysym(69, '€', KeyEvent.KEY_LOCATION_STANDARD));

		// ü
		Assert.assertEquals(XKeySymConstants.XK_udiaeresis, KeycodeConverter.getKeysym(0, 'ü', KeyEvent.KEY_LOCATION_STANDARD));

		// Pos1
		Assert.assertEquals(XKeySymConstants.XK_Home, KeycodeConverter.getKeysym(36, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD));

		// period
		Assert.assertEquals(XKeySymConstants.XK_period, KeycodeConverter.getKeysym(KeyEvent.VK_PERIOD, '.', KeyEvent.KEY_LOCATION_STANDARD));
	}

}
