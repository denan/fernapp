package com.fernapp.vmcontroller.executor;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.vmcontroller.encoding.DamageReport;


/**
 * @author Markus
 * 
 */
public class WindowEventRecorder implements WindowEventListener {

	private final BlockingQueue<RecordedCall> calls = new LinkedBlockingQueue<RecordedCall>();

	public RecordedCall waitForCall(int waitMillis) {
		try {
			RecordedCall call = calls.poll(waitMillis, TimeUnit.MILLISECONDS);
			if (call == null) {
				throw new RuntimeException("No call reported");
			}
			return call;
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean hasRecordedCalls() {
		return !calls.isEmpty();
	}

	/**
	 * @see com.fernapp.uacommon.executor.WindowEventListener#onWindowSettingsChange(com.fernapp.raup.windowmanagement.server.WindowSettings,
	 * boolean)
	 */
	@Override
	public void onWindowSettingsChange(WindowSettings windowSettings, boolean isNewWindow) {
		calls.add(new RecordedCall("onWindowSettingsChange", Arrays.asList(windowSettings, isNewWindow)));
	}

	/**
	 * @see com.fernapp.uacommon.executor.WindowEventListener#onWindowDestroyed(java.lang.String)
	 */
	@Override
	public void onWindowDestroyed(String windowId) {
		calls.add(new RecordedCall("onDestroyed", Arrays.asList(windowId)));
	}

	/**
	 * @see com.fernapp.uacommon.executor.WindowEventListener#onWindowContentUpdate(java.lang.String,
	 * com.fernapp.uacommon.encoding.DamageReport)
	 */
	@Override
	public void onWindowContentUpdate(String windowId, DamageReport damageReport) {
		calls.add(new RecordedCall("onWindowContentUpdate", Arrays.asList(windowId)));
	}

	public static class RecordedCall {
		private String methodName;
		private List<?> arguments;

		public RecordedCall(String methodName, List<?> arguments) {
			this.methodName = methodName;
			this.arguments = arguments;
		}

		public String getMethodName() {
			return methodName;
		}

		public List<?> getArguments() {
			return arguments;
		}
	}

}
