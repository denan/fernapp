package com.fernapp.vmcontroller.executor;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.Assert;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.windowmanagement.client.PointerButtonEvent;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor;
import com.fernapp.uacommon.measurement.DefaultMeasurementReceiver;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.vmcontroller.appsessionservant.ServantOptions;
import com.fernapp.vmcontroller.executor.WindowEventRecorder.RecordedCall;
import com.fernapp.vmcontroller.executor.X11.ProcessManager;


/**
 * @author Markus
 * 
 */
public class X11ApplicationExecutorTest {

	private static final Logger log = LoggerFactory.getLogger(X11ApplicationExecutorTest.class);

	public static final String[] TEST_APP_COMMAND = new String[] {
			"java", "-cp", "target/test-classes", "com.fernapp.vmcontroller.executor.MovingBoxApplication"
	};

	public static final String RUNTIME_RESOURCES_DIR = "./dist";

	@Before
	public void before() throws InterruptedException {
		// sleep a bit to allow Xvfb from a previous test run to shut down completely
		Thread.sleep(2000);
	}

	/**
	 * Performs clicks on the test application, ensures to receive proper events and
	 * captures the content.
	 */
	@Test
	public void clickAndCaptureTask() throws Exception {
		log.info("#clickAndCaptureTask test");

		int repetitions = 10;

		ApplicationExecutor appExecutor = new X11ApplicationExecutor(TEST_APP_COMMAND, false, ServantOptions.DISPLAY_DEFAULT, RUNTIME_RESOURCES_DIR);
		WindowEventRecorder windowEventListener = new WindowEventRecorder();
		AggregatingMeasurementProcessor measurementProcessor = new AggregatingMeasurementProcessor(10);
		MeasurementReceiver measurementReceiver = new DefaultMeasurementReceiver(measurementProcessor);
		appExecutor.init(windowEventListener, measurementReceiver);

		try {
			String windowId;

			// receive initial windowsettings message
			{
				RecordedCall call = windowEventListener.waitForCall(5000);
				Assert.assertEquals("onWindowSettingsChange", call.getMethodName());
				WindowSettings reportedWindowSettings = (WindowSettings) call.getArguments().get(0);
				Assert.assertNotNull(reportedWindowSettings.getWindowId());
				Assert.assertNull(reportedWindowSettings.getParentWindowId());
				Assert.assertEquals(true, call.getArguments().get(1));
				windowId = reportedWindowSettings.getWindowId();
			}

			// receive initial windowcontentupdate
			{
				RecordedCall call = windowEventListener.waitForCall(5000);
				Assert.assertEquals("onWindowContentUpdate", call.getMethodName());
				Assert.assertEquals(windowId, call.getArguments().get(0));
			}

			TimeUnit.MILLISECONDS.sleep(100);
			Assert.assertFalse(windowEventListener.hasRecordedCalls());

			for (int i = 0; i < repetitions; i++) {
				log.info("Performing click");
				PointerButtonEvent input = new PointerButtonEvent(windowId, true, PointerButtonEvent.LEFT_BUTTON);
				input.setPointerPositionLeft(50);
				input.setPointerPositionTop(50);
				appExecutor.sendInputEvent(input);
				TimeUnit.MILLISECONDS.sleep(100);
				input.setButtonPressed(false);
				appExecutor.sendInputEvent(input);

				// receive windowcontentupdate
				RecordedCall call = windowEventListener.waitForCall(1000);
				Assert.assertEquals("onWindowContentUpdate", call.getMethodName());
				Assert.assertEquals(windowId, call.getArguments().get(0));

				// capture content
				appExecutor.captureWindowContent(windowId);

				Assert.assertFalse(windowEventListener.hasRecordedCalls());
			}

			// request close
			appExecutor.requestWindowClose(windowId);

			// receive destroy
			{
				RecordedCall call = windowEventListener.waitForCall(1000);
				Assert.assertEquals("onDestroyed", call.getMethodName());
				Assert.assertEquals(windowId, call.getArguments().get(0));
			}

		} catch (Exception e) {
			log.error("Test failed", e);
			throw e;
		} finally {
			// shutdown
			appExecutor.shutdown();
		}

		log.info("Capturing took in avg. " + measurementProcessor.generateReport(DelayCategory.CAPTURE).getAvgDelay() + "ms");
		Assert.assertFalse(windowEventListener.hasRecordedCalls());
	}

	/**
	 * Test if native mutex also works from the java world.
	 */
	@Test
	public void testNativeMutex() throws Exception {
		log.info("#testNativeMutex test");

		WindowEventListener windowEventListener = EasyMock.createMock(WindowEventListener.class);
		MeasurementReceiver mr = EasyMock.createMock(MeasurementReceiver.class);
		final ApplicationExecutor appExecutor = new X11ApplicationExecutor(X11ApplicationExecutor.DEFAULT_APPLICATION, false, ServantOptions.DISPLAY_DEFAULT, RUNTIME_RESOURCES_DIR);
		appExecutor.init(windowEventListener, mr);

		appExecutor.lockWindowData();
		try {
			final AtomicInteger i = new AtomicInteger(0);

			Executor executor = Executors.newSingleThreadExecutor();
			executor.execute(new Runnable() {
				public void run() {
					appExecutor.lockWindowData();
					i.incrementAndGet();
					appExecutor.unlockWindowData();
				}
			});

			TimeUnit.MILLISECONDS.sleep(1000);

			// worker thread should still be blocked
			Assert.assertEquals(0, i.get());
		} finally {
			appExecutor.unlockWindowData();
		}

		// sleep is necessary to prevent bugs - see #linuxCrasher
		TimeUnit.SECONDS.sleep(1);
		appExecutor.shutdown();
	}

	/**
	 * Crashes Linux after some iteration. Bug could be in our code, the JVM or the Linux
	 * kernel. Fixed by adding a delay into the process stop chain.
	 */
	@Test
	public void linuxCrasher() throws Exception {
		log.info("#linuxCrasher test");

		ProcessManager processManager = new ProcessManager(":5");
		for (int i = 0; i < 2; i++) {
			log.info("Starting Xvfb server");
			processManager.startDaemonProcess(LoggerFactory.getLogger("Xvfb"), "/usr/bin/Xvfb", //
					":5", "-screen", "0", "1600x900x24");

//			log.info("Starting xsettingsd");
//			processManager.startDaemonProcess(LoggerFactory.getLogger("xsettingsd"), RUNTIME_RESOURCES_DIR + "/xsettingsd", "-c", RUNTIME_RESOURCES_DIR + "/xsettingsd.conf");

			log.info("Setting keymap");
			processManager.startDaemonProcess(LoggerFactory.getLogger("setxkbmap"), "setxkbmap", "de");

			log.info("Starting " + X11ApplicationExecutor.DEFAULT_APPLICATION[0]);
			processManager.startDaemonProcess(LoggerFactory.getLogger("App"), X11ApplicationExecutor.DEFAULT_APPLICATION);

			TimeUnit.MILLISECONDS.sleep(100);
			processManager.stop();
		}
	}

}
