package com.fernapp.vmcontroller.raupserver;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.handshake.ClientGreeting;
import com.fernapp.raup.windowmanagement.server.WindowDestroyed;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.Measurement;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.uacommon.middleware.JavaSerMessagingConnection;
import com.fernapp.uacommon.middleware.JsonMessagingConnection;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.uacommon.middleware.channel.DataChannel;
import com.fernapp.uacommon.middleware.channel.SocketDataChannel;
import com.fernapp.util.Callback;
import com.fernapp.util.NamedThreadFactory;
import com.fernapp.vmcontroller.encoding.DamageReport;
import com.fernapp.vmcontroller.executor.ApplicationExecutor;
import com.fernapp.vmcontroller.executor.WindowEventListener;


/**
 * A RAUP server. Manages the RAUP connections of all clients of this RAUP session.
 * <p>
 * Implements {@link WindowEventListener} to collect events from the underlying
 * {@link ApplicationExecutor} and forwards these events to all registered
 * {@link ServerRaupConnection}s.
 * @author Markus
 */
public class RaupServer implements WindowEventListener {

	private static final Logger log = LoggerFactory.getLogger(RaupServer.class);

	private final ApplicationExecutor applicationExecutor;
	// CopyOnWrite makes it unnecessary to use locks which can cause dead locks
	private final List<ServerRaupConnection> serverRaupConnections = new CopyOnWriteArrayList<ServerRaupConnection>();
	private final Executor eventDistributionExecutor;
	private final AtomicInteger nextRaupConnectionId = new AtomicInteger(0);
	private boolean firstWindow = true;

	public RaupServer(ApplicationExecutor applicationExecutor) {
		this.applicationExecutor = applicationExecutor;

		eventDistributionExecutor = Executors.newFixedThreadPool(1, new NamedThreadFactory("eventDistributionExecutor"));

		applicationExecutor.init(this, new MeasurementMulticast());
	}

	public ServerRaupConnection startClientHandshake(Socket socket, boolean jsonMc) throws IOException {
		final int raupConnectionId = nextRaupConnectionId.getAndIncrement();
		String connectionName = "raup" + raupConnectionId;
		DataChannel dataChannel = new SocketDataChannel(socket);

		MessagingConnection messagingConnection;
		if (jsonMc) {
			messagingConnection = new JsonMessagingConnection(dataChannel, connectionName);
		} else {
			messagingConnection = new JavaSerMessagingConnection(dataChannel, connectionName);
		}

		final ServerRaupConnection raupConnection = new DefaultServerRaupConnection(messagingConnection, applicationExecutor, raupConnectionId);

		log.info("Handshaking... Waiting for greeting from client");
		messagingConnection.registerReceivedHandler(ClientGreeting.class, new Callback<ClientGreeting>() {
			@Override
			public void onCallback(final ClientGreeting clientGreeting) throws Exception {
				Runnable clientSetup = new Runnable() {
					public void run() {
						try {
							boolean accepted = raupConnection.processHandshake(clientGreeting);
							if (accepted) {
								addClient(raupConnection);
							}
						} catch (Exception e) {
							log.error("RAUP setup has failed", e);
							raupConnection.close();
						}
					}
				};
				new Thread(clientSetup, "raup" + raupConnectionId + "Setup").start();
			}
		});
		messagingConnection.startReceivingMessages();

		return raupConnection;
	}

	/**
	 * Adds a new RAUP connection to the manager.
	 */
	private void addClient(ServerRaupConnection raupConnection) throws IOException {
		// there is a delay until events are delivered to the new connection. A sync on
		// the windows data (and all event processing methods) will guarantee a
		// consistent view for the ServerRaupConnection during initialization (see
		// raupConnection#initialize)

		applicationExecutor.lockWindowData();
		try {
			raupConnection.initialize();
			// potential concurrency problem: there could be queued old events - maybe
			// block until task queue is empty
			serverRaupConnections.add(raupConnection);
		} finally {
			applicationExecutor.unlockWindowData();
		}

		// can be done outside the synchronization block
		raupConnection.startUsage();
	}

	/**
	 * Closes all RAUP connections and issues a shutdown of the executor.
	 */
	public void shutdown() {
		for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
			serverRaupConnection.close();
		}
		serverRaupConnections.clear();

		applicationExecutor.shutdown();
	}

	/**
	 * @see com.fernapp.uacommon.executor.WindowEventListener#onWindowSettingsChange(com.fernapp.raup.windowmanagement.server.WindowSettings,
	 * boolean)
	 */
	public void onWindowSettingsChange(final WindowSettings windowSettings, final boolean isNewWindow) {
		try {
			eventDistributionExecutor.execute(new Runnable() {
				public void run() {
					if (log.isDebugEnabled()) {
						log.debug("onWindowSettingsChange: " + (isNewWindow?"NEW ":"")+ windowSettings.toString());
					}
					
					if(firstWindow) {
						logServerRunning();
						firstWindow = false;
					}

					try {
						for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
							if (serverRaupConnection.isOpen()) {
								serverRaupConnection.onWindowSettingsChange(windowSettings, isNewWindow);
							} else {
								serverRaupConnections.remove(serverRaupConnection);
							}
						}
					} catch (Exception e) {
						log.error("onWindowSettingsChange failed", e);
					}
				}
			});
		} catch (Exception e) {
			log.error("onWindowSettingsChange task submission failed", e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.executor.WindowEventListener#onWindowDestroyed(int)
	 */
	public void onWindowDestroyed(final String windowId) {
		try {
			eventDistributionExecutor.execute(new Runnable() {
				public void run() {
					if (log.isDebugEnabled()) {
						log.debug("onDestroyed: windowId " + windowId);
					}

					try {
						WindowDestroyed windowDestroyed = new WindowDestroyed(windowId);
						for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
							if (serverRaupConnection.isOpen()) {
								serverRaupConnection.onWindowDestroyed(windowDestroyed);
							} else {
								serverRaupConnections.remove(serverRaupConnection);
							}
						}
					} catch (Exception e) {
						log.error("onDestroyed failed", e);
					}
				}
			});
		} catch (Exception e) {
			log.error("onDestroyed task submission failed", e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.executor.WindowEventListener#onWindowContentUpdate(java.lang.String,
	 * com.fernapp.uacommon.encoding.DamageReport)
	 */
	@Override
	public void onWindowContentUpdate(final String windowId, final DamageReport damageReport) {
		try {
			eventDistributionExecutor.execute(new Runnable() {
				public void run() {
					if (log.isTraceEnabled()) {
						log.trace("onWindowContentUpdate: windowId " + windowId);
					}

					try {
						for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
							if (serverRaupConnection.isOpen()) {
								serverRaupConnection.onWindowContentUpdate(windowId, damageReport);
							} else {
								serverRaupConnections.remove(serverRaupConnection);
							}
						}
					} catch (Exception e) {
						log.error("onWindowContentUpdate failed", e);
					}
				}
			});
		} catch (Exception e) {
			log.error("onWindowContentUpdate task submission failed", e);
		}
	}

	/**
	 * Sends measurements to all RAUP connections.
	 */
	private class MeasurementMulticast implements MeasurementReceiver {

		/**
		 * @see com.fernapp.uacommon.measurement.MeasurementReceiver#receiveMeasurement(com.fernapp.uacommon.measurement.DelayCategory,
		 * long, long)
		 */
		public void receiveMeasurement(DelayCategory delayCategory, long delay, long dateSize) {
			Measurement measurement = new Measurement(System.currentTimeMillis(), delayCategory, delay, dateSize);
			receiveMeasurement(measurement);
		}

		/**
		 * @see com.fernapp.uacommon.measurement.MeasurementReceiver#receiveMeasurement(com.fernapp.uacommon.measurement.Measurement)
		 */
		public void receiveMeasurement(Measurement measurement) {
			for (ServerRaupConnection serverRaupConnection : serverRaupConnections) {
				if (serverRaupConnection.isOpen()) {
					serverRaupConnection.getMeasurementReceiver().receiveMeasurement(measurement);
				} else {
					serverRaupConnections.remove(serverRaupConnection);
				}
			}
		}

	}
	
	private void logServerRunning() {
		String s = "\n\n";
		s += "=========================================================================\n";
		s += "   fernapp server is RUNNING and the application has started\n";
		s += "=========================================================================\n";
		log.info(s);
	}

}
