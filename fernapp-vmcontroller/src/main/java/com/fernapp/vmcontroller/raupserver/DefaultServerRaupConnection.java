package com.fernapp.vmcontroller.raupserver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.Version;
import com.fernapp.raup.VideoEncodingType;
import com.fernapp.raup.handshake.ClientGreeting;
import com.fernapp.raup.handshake.GreetingResponse;
import com.fernapp.raup.measurement.RemoteMeasurements;
import com.fernapp.raup.quality.ReceiveReport;
import com.fernapp.raup.windowmanagement.client.InputEvent;
import com.fernapp.raup.windowmanagement.client.WindowCloseRequest;
import com.fernapp.raup.windowmanagement.client.WindowResizeRequest;
import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;
import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;
import com.fernapp.raup.windowmanagement.server.WindowDestroyed;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor;
import com.fernapp.uacommon.measurement.AggregatingMeasurementProcessor.MeasurementReport;
import com.fernapp.uacommon.measurement.DefaultMeasurementReceiver;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.util.Callback;
import com.fernapp.vmcontroller.encoding.DamageReport;
import com.fernapp.vmcontroller.encoding.DefaultEncoderManagement;
import com.fernapp.vmcontroller.encoding.Encoder;
import com.fernapp.vmcontroller.encoding.EncoderManagement;
import com.fernapp.vmcontroller.executor.ApplicationExecutor;
import com.fernapp.vmcontroller.quality.QualityFeedbackService;
import com.google.common.base.Preconditions;


/**
 * @author Markus
 * 
 */
public class DefaultServerRaupConnection implements ServerRaupConnection {

	private static final Logger log = LoggerFactory.getLogger(DefaultServerRaupConnection.class);

	// a bit hacky - but okay for the moment
	public static String passphrase;

	// injected
	private final MessagingConnection messagingConnection;
	private final ApplicationExecutor applicationExecutor;
	private final int raupConnectionId;

	private final MeasurementReceiver measurementReceiver;
	private final AggregatingMeasurementProcessor measurementProcessor;
	private final AggregatingMeasurementProcessor csvMeasurementProcessor;
	private final EncoderManagement encoderManagement;
	private final QualityFeedbackService qualityFeedbackService;
	private volatile State _state = State.HANDSHAKE_REQUIRED;
	private final Object stateChangeLock = new Object();
	private VideoEncodingType preferedVideoEncoding;

	public DefaultServerRaupConnection(MessagingConnection messagingConnection, ApplicationExecutor _applicationExecutor, int raupConnectionId) {
		this.messagingConnection = messagingConnection;
		this.applicationExecutor = _applicationExecutor;
		this.raupConnectionId = raupConnectionId;

		log.info("Receiving RAUP connection from client " + messagingConnection.getRemoteEndpointDescriptor() + " (RAUP connection ID " + raupConnectionId + ")");

		measurementProcessor = new AggregatingMeasurementProcessor(10);
		csvMeasurementProcessor = new AggregatingMeasurementProcessor(1);
		measurementReceiver = new DefaultMeasurementReceiver(measurementProcessor, csvMeasurementProcessor);
		encoderManagement = new DefaultEncoderManagement(applicationExecutor, measurementReceiver);
		qualityFeedbackService = new QualityFeedbackService(measurementReceiver);

		// input handler
		messagingConnection.registerReceivedHandler(InputEvent.class, new Callback<InputEvent>() {
			public void onCallback(InputEvent payload) {
				applicationExecutor.sendInputEvent(payload);
			}
		});

		// window resizing
		messagingConnection.registerReceivedHandler(WindowResizeRequest.class, new Callback<WindowResizeRequest>() {
			public void onCallback(WindowResizeRequest payload) {
				applicationExecutor.requestWindowResize(payload);
			}
		});

		// window closing
		messagingConnection.registerReceivedHandler(WindowCloseRequest.class, new Callback<WindowCloseRequest>() {
			public void onCallback(WindowCloseRequest payload) {
				applicationExecutor.requestWindowClose(payload.getWindowId());
			}
		});

		// client measurement handler
		messagingConnection.registerReceivedHandler(RemoteMeasurements.class, new Callback<RemoteMeasurements>() {
			public void onCallback(RemoteMeasurements payload) {
				measurementReceiver.receiveMeasurement(payload.getMeasurement());
			}
		});

		// quality feedback
		messagingConnection.registerReceivedHandler(ReceiveReport.class, new Callback<ReceiveReport>() {
			public void onCallback(ReceiveReport payload) {
				// TODO reactivate when adrian's latency measurement problem is fixed
				// qualityFeedbackService.handleReceiveReport(payload);
			}
		});
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#processHandshake(com.fernapp.raup.handshake.ClientGreeting)
	 */
	@Override
	public boolean processHandshake(ClientGreeting clientGreeting) {
		Preconditions.checkState(getState() == State.HANDSHAKE_REQUIRED);
		log.info("Processing handshake");

		if (clientGreeting.getProtocolVersion() != Version.RAUP_PROTOCOL_VERSION) {
			rejectClient("Expected RAUP version " + Version.RAUP_PROTOCOL_VERSION + " but was " + clientGreeting.getProtocolVersion());
			return false;
		}

		if (!passphrase.equals(clientGreeting.getPassphrase())) {
			rejectClient("Passphrase incorrect");
			return false;
		}

		// accept
		preferedVideoEncoding = clientGreeting.getPreferedVideoEncoding();
		messagingConnection.sendMessageAsync(new GreetingResponse(true, null));
		setState(State.HANDSHAKE_ACCEPTED);
		return true;
	}

	private void rejectClient(String reason) {
		log.warn("Rejecting client. " + reason);
		try {
			messagingConnection.sendMessage(new GreetingResponse(false, reason));
		} catch (IOException e) {
			log.error("Failed to send handshake rejection", e);
		}
		close();
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#initialize()
	 */
	@Override
	public void initialize() {
		Preconditions.checkState(getState() == State.HANDSHAKE_ACCEPTED);
		log.info("Initializing");

		// get all current windows and send current state to client and register encoders
		Collection<WindowSettings> openWindows = applicationExecutor.getOpenWindows();
		for (WindowSettings windowSettings : openWindows) {
			onWindowSettingsChange(windowSettings, true);
		}
		if (openWindows.isEmpty()) {
			log.warn("There are no open windows");
		}

		setState(State.INITIALIZED);
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#startUsage()
	 */
	@Override
	public void startUsage() throws IOException {
		Preconditions.checkState(getState() == State.INITIALIZED);
		log.info("Starting usage of RAUP connection");

		// must be done after startReceivingMessages
		// connectionWarmUp is only need for performance tests
		// qualityFeedbackService.connectionWarmUp(messagingConnection);
		// TODO reactivate when adrian's latency measurement problem is fixed
		// qualityFeedbackService.determinePacketDelay(messagingConnection);

		// start send loop, encoders are registered at this point
		new Thread(new Runnable() {
			public void run() {
				try {
					sendLoop();
				} catch (Exception e) {
					log.error("RAUP send loop has died", e);
				}
			}
		}, messagingConnection.getName() + "SendLoop").start();

		// measurement reporter
		// TODO reactivate when adrian's latency measurement problem is fixed
		// new Thread(new Runnable() {
		// public void run() {
		// try {
		// doReports();
		// } catch (IOException e) {
		// log.error("Measurement reporter failed", e);
		// }
		// }
		// }, messagingConnection.getName() + "Reports").start();

		setState(State.ACTIVE);
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#close()
	 */
	@Override
	public void close() {
		// triggers the actual closing of the RAUP session in #sendLoop
		messagingConnection.close();
	}

	/**
	 * Loop that sends {@link WindowContentUpdate}s to the client while the connection is
	 * open. Additional resize events are not required because the information is included
	 * in the {@link VideoStreamChunk}s. Flow control is done by the blocking
	 * {@link MessagingConnection#sendMessage(Object)} method.
	 */
	private void sendLoop() {
		while (messagingConnection.isOpen()) {
			Encoder encoder = encoderManagement.getNextEncoderWithUpdate();
			if (encoder != null) {
				VideoStreamChunk videoStreamChunk = encoder.getEncodedChunk();
				if (videoStreamChunk != null) {
					// this window has a change
					WindowContentUpdate contentUpdate = new WindowContentUpdate(encoder.getWindowId(), videoStreamChunk);
					qualityFeedbackService.prepareForQoS(contentUpdate);
					try {
						messagingConnection.sendMessage(contentUpdate);
					} catch (IOException e) {
						if (messagingConnection.isOpen()) {
							log.debug("Failed to send content update", e);
						}
						// else: ignore
					}
				} else {
					log.warn("Expected that the encoder has an update");
					// might happen if the window and the encoder were removed
					// in-between
				}
			}
		}

		// connection has been closed
		setState(State.CLOSED);
		if (messagingConnection.isOpen()) {
			throw new IllegalStateException("Connection was expected to be closed");
		}

		// unregister all encoders
		encoderManagement.removeAllEncoders();

		// log performance report
		measurementProcessor.logMeasurements();

		log.info("RAUP connection is closed");
	}

	public boolean isOpen() {
		return messagingConnection.isOpen();
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#onWindowSettingsChange(WindowSettings,
	 * boolean)
	 */
	public void onWindowSettingsChange(WindowSettings windowSettings, boolean isNewWindow) {
		synchronized (stateChangeLock) {
			if (getState() == State.HANDSHAKE_ACCEPTED || getState() == State.INITIALIZED || getState() == State.ACTIVE) {
				log.debug("Sending window settings for window " + windowSettings.getWindowId());
				messagingConnection.sendMessageAsync(windowSettings);

				// register encoders
				if (isNewWindow) {
					VideoEncodingType videoEncodingType = preferedVideoEncoding;
					int quality = 50;
					encoderManagement.setEncoder(windowSettings.getWindowId(), videoEncodingType, quality);
				}
			} else {
				log.warn("Window settings ignored because state is " + getState());
			}
		}
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#onWindowDestroyed(com.fernapp.raup.windowmanagement.server.WindowDestroyed)
	 */
	public void onWindowDestroyed(WindowDestroyed windowDestroyed) {
		synchronized (stateChangeLock) {
			if (getState() == State.HANDSHAKE_ACCEPTED || getState() == State.INITIALIZED || getState() == State.ACTIVE) {
				messagingConnection.sendMessageAsync(windowDestroyed);

				// remove encoder
				encoderManagement.removeEncoder(windowDestroyed.getWindowId());
			} else {
				log.warn("Window destroy ignored because state is " + getState());
			}
		}
	}

	/**
	 * @see com.fernapp.uacommon.raupserver.ServerRaupConnection#onWindowContentUpdate(String,
	 * com.fernapp.uacommon.encoding.DamageReport)
	 */
	public void onWindowContentUpdate(String windowId, DamageReport damageReport) {
		encoderManagement.onWindowUpdated(windowId, damageReport);
	}

	@SuppressWarnings("unused")
	private void doReports() throws IOException {
		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(new File("measurements-" + messagingConnection.getName() + ".csv")), "UTF-8");

		for (DelayCategory delayCategory : DelayCategory.values()) {
			writer.write(delayCategory.toString());
			writer.write(";");
		}
		writer.write("\r\n");

		while (isOpen()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				log.error("reportUpdater failed", e);
			}

			// getMeasurementProcessor().logMeasurements();

			// add to CSV
			for (DelayCategory delayCategory : DelayCategory.values()) {
				MeasurementReport mr = csvMeasurementProcessor.generateReport(delayCategory);
				if (mr != null) {
					writer.write(String.valueOf((int) mr.getAvgDelay()));
				} else {
					// empty value
				}
				writer.write(";");
			}
			writer.write("\r\n");
			writer.flush();
		}

		writer.close();
	}

	/**
	 * @return the qualityFeedbackService
	 */
	public QualityFeedbackService getQualityFeedbackService() {
		return qualityFeedbackService;
	}

	/**
	 * @return the measurementReceiver
	 */
	public MeasurementReceiver getMeasurementReceiver() {
		return measurementReceiver;
	}

	/**
	 * @return the measurementProcessor
	 */
	public AggregatingMeasurementProcessor getMeasurementProcessor() {
		return measurementProcessor;
	}

	/**
	 * Returns the state (is thread-safe).
	 */
	@Override
	public State getState() {
		return _state;
	}

	/**
	 * Changes the state (is thread-safe).
	 */
	public void setState(State newState) {
		synchronized (stateChangeLock) {
			if (log.isDebugEnabled()) {
				log.debug("Changing state from " + getState().toString() + " to " + newState.toString() + " (RAUP connection ID " + raupConnectionId + ")");
			}
			this._state = newState;
		}
	}

}
