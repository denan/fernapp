package com.fernapp.vmcontroller.quality;

import java.io.IOException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.quality.EchoRequest;
import com.fernapp.raup.quality.EchoResponse;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.util.Callback;


/**
 * Measures the time a very short message requires to reach the destination. It is the
 * minimum time the middleware needs to transport a message.
 * @author Markus
 */
public class PacketDelayMeasurer implements Callback<EchoResponse> {

	private static final Logger log = LoggerFactory.getLogger(PacketDelayMeasurer.class);

	private final MessagingConnection messagingConnection;
	private final CyclicBarrier receiveBarrier = new CyclicBarrier(2);
	private int probeCount;

	// these variables are modified by the receive thread (memory visibility)!
	private Long minPacketDelay;
	private Long maxPacketDelay;
	private long sumPacketDelay;
	private Long avgPacketDelay;

	private volatile long currentEchoSendTime;

	public PacketDelayMeasurer(MessagingConnection messagingConnection) {
		this.messagingConnection = messagingConnection;
	}

	public synchronized void measure(int _probeCount) throws IOException {
		this.probeCount = _probeCount;
		minPacketDelay = null;
		maxPacketDelay = null;
		sumPacketDelay = 0;
		avgPacketDelay = null;

		// register echo receiver
		messagingConnection.registerReceivedHandler(EchoResponse.class, this);

		for (int i = 0; i < probeCount; i++) {
			log.trace("Sending EchoRequest");
			currentEchoSendTime = System.currentTimeMillis();
			messagingConnection.sendMessage(new EchoRequest());
			try {
				receiveBarrier.await(10, TimeUnit.SECONDS);
			} catch (Exception e) {
				throw new IOException("Client failed to respond to echo request in time", e);
			}
		}

		// all echos have been processed at this point and all memory changes are visible
		// due to await

		avgPacketDelay = Math.round((double) sumPacketDelay / probeCount);
		assert (avgPacketDelay >= minPacketDelay);
		assert (avgPacketDelay <= maxPacketDelay);
	}

	/**
	 * @see com.fernapp.util.Callback#onCallback(java.lang.Object)
	 */
	public void onCallback(EchoResponse payload) throws Exception {
		long roundTripTime = System.currentTimeMillis() - currentEchoSendTime;
		log.trace("Received a EchoResponse");
		long packetDelay = roundTripTime / 2;
		addDelayMeasurement(packetDelay);
		// unblock the sender
		receiveBarrier.await(10, TimeUnit.SECONDS);
	}

	private void addDelayMeasurement(long packetDelay) {
		if (sumPacketDelay == 0) {
			// first echo
			minPacketDelay = packetDelay;
			maxPacketDelay = packetDelay;
		} else {
			minPacketDelay = Math.min(minPacketDelay, packetDelay);
			maxPacketDelay = Math.max(maxPacketDelay, packetDelay);
		}
		sumPacketDelay += packetDelay;
	}

	public String printResults() {
		return "Measured a minimum message delay of " + avgPacketDelay + "ms (probe count: " + probeCount + ", min: " + minPacketDelay + "ms, max: " + maxPacketDelay + "ms";
	}

	public Long getAvgPacketDelay() {
		return avgPacketDelay;
	}

}
