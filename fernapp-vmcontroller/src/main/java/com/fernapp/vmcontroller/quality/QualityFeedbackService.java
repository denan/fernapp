package com.fernapp.vmcontroller.quality;

import java.io.IOException;
import java.util.concurrent.TimeUnit;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.quality.DummyData;
import com.fernapp.raup.quality.ReceiveReport;
import com.fernapp.raup.windowmanagement.server.WindowContentUpdate;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.google.common.base.Preconditions;


/**
 * Service to measures quality-of-service parameters.
 * @author Markus
 */
public class QualityFeedbackService {

	private static final Logger log = LoggerFactory.getLogger(QualityFeedbackService.class);

	private final MeasurementReceiver measurementReceiver;
	private transient Long packetDelay;
	protected transient Long lastUpdateEndToEndDelay;

	public QualityFeedbackService() {
		this.measurementReceiver = null;
	}

	public QualityFeedbackService(MeasurementReceiver measurementReceiver) {
		Preconditions.checkNotNull(measurementReceiver);
		this.measurementReceiver = measurementReceiver;
	}

	public void prepareForQoS(WindowContentUpdate windowContentUpdate) {
		windowContentUpdate.setServerSendTime(System.nanoTime());
	}

	/**
	 * Handles the {@link ReceiveReport} for {@link WindowContentUpdate}s.
	 */
	public void handleReceiveReport(ReceiveReport receiveReport) {
		if (packetDelay == null) {
			throw new IllegalStateException("Packet delay unkown");
		}

		long roundtripNanos = System.nanoTime() - receiveReport.getServerSendTime();
		long roundtripMillis = TimeUnit.MILLISECONDS.convert(roundtripNanos, TimeUnit.NANOSECONDS);
		long updateEndToEndDelay = roundtripMillis - packetDelay;
		lastUpdateEndToEndDelay = updateEndToEndDelay;

		if (log.isTraceEnabled()) {
			log.trace("Measured WindowContentUpdate transport time: " + updateEndToEndDelay + "ms");
		}
		if (measurementReceiver != null) {
			measurementReceiver.receiveMeasurement(DelayCategory.MIDDLEWARE, updateEndToEndDelay, 0);
		}
	}

	public void determinePacketDelay(MessagingConnection messagingConnection) throws IOException {
		log.debug("Measuring packet delay...");

		PacketDelayMeasurer measurer = new PacketDelayMeasurer(messagingConnection);
		// send a few dummy echos to warm up
		measurer.measure(1);
		// now lets do the real measurement
		measurer.measure(3);

		log.info(measurer.printResults());
		packetDelay = measurer.getAvgPacketDelay();
	}

	/**
	 * Sends some dummy data over the connection to overcome TCP slow start.
	 */
	public void connectionWarmUp(MessagingConnection messagingConnection) throws IOException {
		log.info("Connection warm up");
		DummyData dummyData = new DummyData(new byte[200000]);
		for (int i = 0; i < 8; i++) {
			messagingConnection.sendMessage(dummyData);
		}
		log.info("Connection warm up finished");
	}

	/**
	 * @return the packetDelay
	 */
	public Long getPacketDelay() {
		return packetDelay;
	}

}
