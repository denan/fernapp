package com.fernapp.vmcontroller.executor.X11;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Joiner;


/**
 * Setup of the X server environment.
 * @author Markus
 */
public class ProcessManager {

	private static final Logger log = LoggerFactory.getLogger(ProcessManager.class);

	private final String displayPort;
	private final Deque<DaemonProcess> processes = new LinkedList<DaemonProcess>();
	private final Executor executor = Executors.newCachedThreadPool();

	public ProcessManager(String displayPort) {
		this.displayPort = displayPort;
	}

	public void stop() {
		synchronized (processes) {
			for (DaemonProcess dp : processes) {
				log.info("Terminating process " + dp.getName());
				try {
					dp.getProcess().destroy();
					dp.getProcess().waitFor();
					Thread.sleep(500);
				} catch (Throwable e) {
					log.error("Failed to kill process", e);
				}
			}
			processes.clear();
		}
	}

	public Process startDaemonProcess(final Logger logger, String... command) throws IOException {
		if(log.isDebugEnabled()) {
			log.debug("Starting daemon process: " + Joiner.on(" ").join(command));
		}
		
		ProcessBuilder builder = new ProcessBuilder(command);
		builder.redirectErrorStream(true);
		builder.environment().put("DISPLAY", displayPort);
		final Process process = builder.start();

		synchronized (processes) {
			processes.push(new DaemonProcess(command[0], process));
		}

		executor.execute(new Runnable() {
			public void run() {
				try {
					BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
					String line;
					while ((line = reader.readLine()) != null) {
						logger.info(line);
					}
					reader.close();
				} catch (IOException e) {
					// ignore - happens when stream gets closed
				} catch (Exception e) {
					log.error("Reading process output failed", e);
				}
			}
		});

		return process;
	}

	public static class DaemonProcess {
		private String name;
		private Process process;

		public DaemonProcess(String name, Process process) {
			this.name = name;
			this.process = process;
		}

		public String getName() {
			return name;
		}

		public Process getProcess() {
			return process;
		}
	}

}
