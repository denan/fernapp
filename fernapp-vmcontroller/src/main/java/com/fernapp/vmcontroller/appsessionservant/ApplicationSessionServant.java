package com.fernapp.vmcontroller.appsessionservant;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.channel.SocketFactory;
import com.fernapp.vmcontroller.executor.ApplicationExecutor;
import com.fernapp.vmcontroller.executor.X11ApplicationExecutor;
import com.fernapp.vmcontroller.httpserver.AppletHttpServer;
import com.fernapp.vmcontroller.raupserver.DefaultServerRaupConnection;
import com.fernapp.vmcontroller.raupserver.RaupServer;


/**
 * Starts the application. Listens for incoming RAUP connections.
 * @author Markus
 * 
 */
public class ApplicationSessionServant {

	private static final Logger log = LoggerFactory.getLogger(ApplicationSessionServant.class);

	public static void main(String[] args) {
		try {
			Thread.currentThread().setName("as-servant");

			// parse options
			ServantOptions options = new ServantOptions();
			CmdLineParser parser = new CmdLineParser(options);
			try {
				parser.parseArgument(args);
			} catch (CmdLineException e) {
				System.err.println(e.getMessage());
				parser.printUsage(System.err);
				return;
			}

			new ApplicationSessionServant().startRaupServer(options);
		} catch (Exception e) {
			log.error("Application Session Servant is dying", e);
		}
	}

	private void startRaupServer(ServantOptions options) throws IOException {
		log.info("Starting application session RAUP server on port " + options.getPort());

		// initialize services and start application
		String[] cmd = options.getCommand().toArray(new String[]{});
		ApplicationExecutor executor = new X11ApplicationExecutor(cmd, options.isClassicX(), options.getDisplay(), options.getExternalResourcesPath());
		final RaupServer raupServer = new RaupServer(executor);
		DefaultServerRaupConnection.passphrase = options.getPassphrase();
		
		new AppletHttpServer(options.getHttpPort()).init();

		// Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
		// public void run() {
		// log.info("JVM interrupted");
		// try {
		// raupServer.shutdown();
		// } catch (Exception e) {
		// log.error("Shutdown failed", e);
		// }
		// }
		// }));

		// hack: do the same for JSON protocol
		new Thread(new Runnable() {
			public void run() {
				try {
					ServerSocket serverSocket = new ServerSocket(4712);
					while (true) {
						try {
							Socket socket = serverSocket.accept();
							SocketFactory.configureServerSocket(socket);
							raupServer.startClientHandshake(socket, true);
						} catch (IOException e) {
							log.error("IOException while establishing connection with RAUP client", e);
						}
					}
				} catch (IOException e) {
					log.error("Failed to listen for JSON connections", e);
				}
			}
		}, "jsonPort").start();

		// listen for raup connections
		ServerSocket serverSocket = new ServerSocket(options.getPort());
		while (true) {
			try {
				Socket socket = serverSocket.accept();
				SocketFactory.configureServerSocket(socket);
				raupServer.startClientHandshake(socket, false);
			} catch (IOException e) {
				log.error("IOException while establishing connection with RAUP client", e);
			}
		}
	}
}
