package com.fernapp.useragent.swing.ui;

import java.awt.Component;
import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * probably not necessary
 * @author Markus
 * 
 */
public class WindowContainerKeyEventDispatcher implements KeyEventDispatcher {

	// AbstractAwtContainerWrapper:
	// private static final WindowContainerKeyEventDispatcher keyEventDispatcher;
	// static {
	// keyEventDispatcher = new WindowContainerKeyEventDispatcher();
	// KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(keyEventDispatcher);
	// }

	private static final Logger log = LoggerFactory.getLogger(WindowContainerKeyEventDispatcher.class);

	private final Map<Component, KeyListener> componentToWindowContainerMap = new HashMap<Component, KeyListener>();

	/**
	 * @see java.awt.KeyEventDispatcher#dispatchKeyEvent(java.awt.event.KeyEvent)
	 */
	public boolean dispatchKeyEvent(KeyEvent e) {
		Component comp = e.getComponent();
		KeyListener keyListener = null;
		while (keyListener == null && comp != null) {
			log.debug("comp " + comp);
			keyListener = componentToWindowContainerMap.get(comp);
			comp = comp.getParent();
		}

		if (keyListener != null) {
			switch (e.getID()) {
			case KeyEvent.KEY_PRESSED:
				keyListener.keyPressed(e);
				break;
			case KeyEvent.KEY_RELEASED:
				keyListener.keyReleased(e);
				break;
			case KeyEvent.KEY_TYPED:
				keyListener.keyTyped(e);
				break;
			}

			return true;
		} else {
			log.debug("No KeyListener known for event delivery");
			return false;
		}
	}

	public void addComponent(Component component, KeyListener keyListener) {
		if (componentToWindowContainerMap.containsKey(component)) {
			throw new IllegalStateException("Component already registered");
		}
		componentToWindowContainerMap.put(component, keyListener);
	}

	public void removeComponent(Component component) {
		KeyListener keyListener = componentToWindowContainerMap.remove(component);
		if (keyListener == null) {
			throw new IllegalStateException("Component was not registered");
		}

	}

}
