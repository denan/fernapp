package com.fernapp.useragent.swing.applet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


/**
 * The main panel of the applet.
 * @author Markus
 */
public class AppletUserInterface extends JPanel {

	private final JPanel titleBar = new JPanel();
	private final JPanel remoteWindowPanel = new JPanel();
	private final JLabel titleLabel = new JLabel();
	private final JPanel controlPanel = new JPanel();

	public AppletUserInterface() {
		setLayout(new BorderLayout());
		add(titleBar, BorderLayout.NORTH);
		add(remoteWindowPanel, BorderLayout.CENTER);

		// remoteWindowPanel will be used by the raup client to paint the remote window
		remoteWindowPanel.setBackground(Color.RED);

		// setup title bar
		titleBar.setBackground(Color.BLACK);
		titleBar.setBorder(new EmptyBorder(2, 15, 2, 20));
		titleBar.setLayout(new BorderLayout());
		titleBar.add(titleLabel, BorderLayout.CENTER);
		titleBar.add(controlPanel, BorderLayout.EAST);

		titleLabel.setFont(new Font("Dialog", Font.PLAIN, 18));
		titleLabel.setForeground(new Color(230, 230, 230));

		controlPanel.setOpaque(false);
		JButton shareButton = new JButton("Share");
		shareButton.setEnabled(false);
		controlPanel.add(shareButton);
		JButton exitButton = new JButton("Exit");
		exitButton.setEnabled(false);
		controlPanel.add(exitButton);
	}

	/**
	 * @return the remoteWindowPanel
	 */
	public JPanel getRemoteWindowPanel() {
		return remoteWindowPanel;
	}

	public void setAppTitle(String title) {
		titleLabel.setText(title);
	}
	
	public void updateStatus(String status) {
		setAppTitle(status);
	}

}
