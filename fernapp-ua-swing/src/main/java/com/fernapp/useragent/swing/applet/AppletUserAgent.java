package com.fernapp.useragent.swing.applet;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;

import javax.swing.JApplet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.useragent.ApplicationSessionConnectionDetails;
import com.fernapp.useragent.DefaultConnectionKeeper;
import com.fernapp.useragent.ServiceHub;
import com.fernapp.useragent.swing.ui.AbstractAwtContainerWrapper;
import com.fernapp.useragent.ui.WindowStructuralListener;
import com.fernapp.util.Callback;


/**
 * Java applet based RAUP client.
 * @author Markus
 */
public class AppletUserAgent extends JApplet {

	private static final Logger log = LoggerFactory.getLogger(AppletUserAgent.class);

	private final AppletRaupClient raupClient;
	private final AppletRemoteWindowContainer windowContainer;
	private final AppletUserInterface userInterface;

	public AppletUserAgent() {
		userInterface = new AppletUserInterface();
		windowContainer = new AppletRemoteWindowContainer(userInterface.getRemoteWindowPanel());
		raupClient = new AppletRaupClient(windowContainer);
		ServiceHub.connectionKeeper = new DefaultConnectionKeeper();
	}

	public void init() {
		Thread.currentThread().setName("main");
		log.info("Initializing");

		ComponentListener compl = new ComponentAdapter() {
			public void componentShown(ComponentEvent e) {
				raupClient.setBrowserWindowSize(new Dimension(windowContainer.getContentWidth(), windowContainer.getContentHeight()));
			}
		};
		addComponentListener(compl);

		setContentPane(userInterface);
		userInterface.getRemoteWindowPanel().requestFocus();

		// @SuppressWarnings("restriction")
		// JSObject window = JSObject.getWindow(this);
		// window.eval("alert('it works 2');");

		userInterface.updateStatus("Waiting for connection...");
		try {
			new Thread(new Runnable() {
				public void run() {
					connectionKeeper();
				}
			}, "connectionKeeper").start();
		} catch (Exception e) {
			log.error("Failed to initialize applet", e);
		}
	}

	private void connectionKeeper() {
		String host = getDocumentBase().getHost();
		ApplicationSessionConnectionDetails connectionDetails = new ApplicationSessionConnectionDetails(host, 4711);

		// connectionKeeper
		ServiceHub.connectionKeeper.addConnectionChangeListener(new Callback<MessagingConnection>() {
			public void onCallback(MessagingConnection mc) {
				raupClient.initForConnection(mc);
				
				if(mc==null) {
					userInterface.updateStatus("Connection failure");
					stop();
				}
			}
		});
		ServiceHub.connectionKeeper.keepConnection(connectionDetails);
	}

	public void stop() {
		try {
			log.info("Stopping");
			ServiceHub.connectionKeeper.setEnabled(false);
			raupClient.shutdown();
		} catch (Exception e) {
			// we must not throw exceptions here in order to to allow the applet to shutdown
			log.error("Error during shutdown", e);
		}

	}

	public class AppletRemoteWindowContainer extends AbstractAwtContainerWrapper {

		protected AppletRemoteWindowContainer(Container container) {
			super(container);
		}

		/**
		 * @see com.fernapp.useragent.ui.WindowContainer#setTitle(java.lang.String)
		 */
		public void setTitle(String title) {
			userInterface.setAppTitle(title);
		}

		/**
		 * @see com.fernapp.useragent.ui.WindowContainer#dispose()
		 */
		public void dispose() {
			BufferedImage bi = new BufferedImage(getContentWidth(), getContentHeight(), BufferedImage.TYPE_INT_RGB);
			Graphics g = bi.getGraphics();
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getContentWidth(), getContentHeight());
			g.setColor(Color.WHITE);
			g.drawString("Closed", 100, 50);
			updateContent(bi);
		}

		/**
		 * @see com.fernapp.useragent.swing.ui.AbstractAwtContainerWrapper#addWindowCloseListener(com.fernapp.useragent.ui.WindowStructuralListener)
		 */
		@Override
		public void addWindowCloseListener(WindowStructuralListener listener) {
			// TODO Auto-generated method stub
		}

		/**
		 * @see com.fernapp.useragent.swing.ui.AbstractAwtContainerWrapper#adjustContainerSizeToContentPanel()
		 */
		@Override
		protected void adjustContainerSizeToContentPanel() {
			// we don't allow the remote app to change the size of the browser window
		}

	}

}