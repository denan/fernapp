package com.fernapp.useragent.ui;

import java.awt.event.KeyEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.windowmanagement.client.InputEvent;
import com.fernapp.raup.windowmanagement.client.KeyboardInputEvent;
import com.fernapp.raup.windowmanagement.client.PointerButtonEvent;
import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.useragent.RaupClient;
import com.fernapp.useragent.common.ActionDelayMeasurer;


/**
 * We collect input event here. Usually events will be send immediately, but not mouse
 * move events.
 * @author Markus
 */
public class InputEventSender implements InputEventListener {

	private static final Logger log = LoggerFactory.getLogger(InputEventSender.class);

	private String windowId;
	private final RaupClient raupClient;
	private long maxDeliveryDelay = 100;

	private boolean enabled = true;
	private Long requiredDeliveryTime;
	private Integer pointerLeft;
	private Integer pointerTop;

	public InputEventSender(String windowId, RaupClient raupClient) {
		this.windowId = windowId;
		this.raupClient = raupClient;

		new Thread(new DelayedDeliverer(), "delayedEvents" + windowId).start();
	}

	private void sendInputEvent(InputEvent inputEvent) {
		MessagingConnection mc = raupClient.getMessagingConnection();
		if (mc != null) {
			// make it null first, then we can guarantee that we don't miss anything -
			// without synchronization
			requiredDeliveryTime = null;

			// piggyback stuff
			inputEvent.setPointerPositionLeft(pointerLeft);
			inputEvent.setPointerPositionTop(pointerTop);

			mc.sendMessageAsync(inputEvent);
		}
	}

	public void recommendDelivery() {
		if (requiredDeliveryTime == null) {
			requiredDeliveryTime = System.currentTimeMillis() + maxDeliveryDelay;
		}
	}

	public void dispose() {
		enabled = false;
	}

	private class DelayedDeliverer implements Runnable {
		public void run() {
			while (enabled) {
				try {
					if (requiredDeliveryTime != null && requiredDeliveryTime < System.currentTimeMillis()) {
						sendInputEvent(new InputEvent(windowId));
					}
					Thread.sleep(maxDeliveryDelay);
				} catch (InterruptedException e) {
					log.error("Something went wrong", e);
				}
			}
		}
	}

	public void mouseMoved(int x, int y) {
		pointerLeft = x;
		pointerTop = y;
		recommendDelivery();
	}

	public void mousePressed(int button) {
		PointerButtonEvent inputEvent = new PointerButtonEvent(windowId, true, button);
		sendInputEvent(inputEvent);
	}

	public void mouseReleased(int button) {
		ActionDelayMeasurer.INSTANCE.onActionStart();
		PointerButtonEvent inputEvent = new PointerButtonEvent(windowId, false, button);
		sendInputEvent(inputEvent);
	}

	private boolean altGrActive = false;

	public void keyPressed(KeyEvent e) {
		// Java doesn't correctly send VK_ALT_GRAPH but VK_CONTROL+VK_ALT (observed on
		// Windows). We change the right ALT to AltGr (nobody has a right ALT key today
		// anyway)
		// hack:
		if (e.getKeyCode() == KeyEvent.VK_ALT && e.getKeyLocation() == KeyEvent.KEY_LOCATION_RIGHT && e.isControlDown()) {
			// release the control key
			KeyboardInputEvent ke = new KeyboardInputEvent(windowId, KeyEvent.VK_CONTROL, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT, false);
			sendInputEvent(ke);

			// send press AltGr
			ke = new KeyboardInputEvent(windowId, KeyEvent.VK_ALT_GRAPH, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, true);
			sendInputEvent(ke);
			altGrActive = true;
			return;
		}

		KeyboardInputEvent ke = new KeyboardInputEvent(windowId, e.getKeyCode(), e.getKeyChar(), e.getKeyLocation(), true);
		sendInputEvent(ke);
	}

	public void keyReleased(KeyEvent e) {
		if (altGrActive) {
			// first: eat the release of CONTROL
			if (e.getKeyCode() == KeyEvent.VK_CONTROL && e.getKeyLocation() == KeyEvent.KEY_LOCATION_LEFT) {
				// send release AltGr
				KeyboardInputEvent ke = new KeyboardInputEvent(windowId, KeyEvent.VK_ALT_GRAPH, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, false);
				sendInputEvent(ke);
				return;
			}

			// eat the release of ALT
			if (e.getKeyCode() == KeyEvent.VK_ALT && e.getKeyLocation() == KeyEvent.KEY_LOCATION_RIGHT) {
				altGrActive = false;
				return;
			}
		}

		KeyboardInputEvent ke = new KeyboardInputEvent(windowId, e.getKeyCode(), e.getKeyChar(), e.getKeyLocation(), false);
		sendInputEvent(ke);
	}

}
