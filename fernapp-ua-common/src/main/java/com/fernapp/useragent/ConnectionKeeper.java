package com.fernapp.useragent;

import com.fernapp.uacommon.middleware.MessagingConnection;
import com.fernapp.util.Callback;


/**
 * Manages the RAUP connection to the server.
 * @author Markus
 */
public interface ConnectionKeeper {

	/**
	 * Blocks and tries to keep a connection.
	 */
	void keepConnection(ApplicationSessionConnectionDetails connectionDetails);

	void setEnabled(boolean enabled);

	/**
	 * Receiving events will start after all listeners have been informed about the
	 * change.
	 */
	void addConnectionChangeListener(Callback<MessagingConnection> changeCallback);

}
