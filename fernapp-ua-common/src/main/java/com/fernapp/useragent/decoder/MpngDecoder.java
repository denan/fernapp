package com.fernapp.useragent.decoder;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.DataInput;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.VideoEncodingType;
import com.google.common.base.Preconditions;
import com.google.common.io.LittleEndianDataInputStream;


/**
 * A decoder for the MPNG codec. See documentation in mpng_codec.txt.
 * @author Markus
 */
public class MpngDecoder implements Decoder {

	private static final Logger log = LoggerFactory.getLogger(MpngDecoder.class);

	private BufferedImage image;

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#getEncodingType()
	 */
	public VideoEncodingType getEncodingType() {
		return VideoEncodingType.MPNG;
	}

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#decode(int, int, byte[])
	 */
	public BufferedImage decode(int width, int height, byte[] encodedChunk) {

		ByteArrayInputStream in = new ByteArrayInputStream(encodedChunk);
		try {
			// insertion position
			DataInput din = new LittleEndianDataInputStream(in);
			int damageX = din.readInt();
			int damageY = din.readInt();

			BufferedImage newImage = ImageIO.read(in);
			log.info("Decoded MPNG picture (" + damageX + ", " + damageY + ", " + newImage.getWidth() + ", " + newImage.getHeight() + ")");

			if (image == null) {
				// this is the intital image
				Preconditions.checkState(damageX == 0);
				Preconditions.checkState(damageY == 0);
				if (width != newImage.getWidth()) {
					throw new IllegalArgumentException("Expected width of " + width);
				}
				if (height != newImage.getHeight()) {
					throw new IllegalArgumentException("Expected height of " + height);
				}
				image = newImage;
			} else {
				// insert the changed part into the cached image
				BufferedImage oldPart = image.getSubimage(damageX, damageY, newImage.getWidth(), newImage.getHeight());
				newImage.copyData(oldPart.getRaster());
			}

			// for debugging: mark part that has been changed
			// {
			// BufferedImage markedImage = new BufferedImage(image.getColorModel(),
			// image.copyData(null), image.isAlphaPremultiplied(), null);
			// Graphics graphics = markedImage.getGraphics();
			// graphics.setColor(Color.RED);
			// graphics.drawRect(damageX, damageY, newImage.getWidth(),
			// newImage.getHeight());
			// return markedImage;
			// }

			return image;
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				log.error("Failed to close stream", e);
			}
		}
	}

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#shutdown()
	 */
	public void shutdown() {
		// nothing to do
	}

}
