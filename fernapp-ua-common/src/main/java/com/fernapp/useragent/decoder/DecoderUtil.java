package com.fernapp.useragent.decoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * @author Markus
 * 
 */
public class DecoderUtil {

	public static void writeAsPPM(int width, int height, byte[] rgbImage, File file) throws IOException {
		FileOutputStream fos = new FileOutputStream(file);

		fos.write(new String("P6\n").getBytes());
		fos.write(new String(width + " " + height + "\n").getBytes());
		fos.write(new String("255\n").getBytes());

		for (int i = 0; i < rgbImage.length; i++) {
			fos.write(rgbImage[i]);
		}
		fos.close();
	}

	public static void printBits(int x) {
		System.out.print(x + ": ");
		for (int i = 31; i >= 0; i--) {
			int p = 1 << i;
			boolean on = (p & x) != 0;
			if (on) {
				System.out.print("1");
			} else {
				System.out.print("0");
			}
		}
		System.out.print("\n");
	}
}
