package com.fernapp.useragent.decoder;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.WritableRaster;

import com.fernapp.raup.VideoEncodingType;



/**
 * Creates a {@link BufferedImage} out of 3-byte RGB data.
 * @author Markus
 */
public class RawDecoder implements Decoder {

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#getEncodingType()
	 */
	public VideoEncodingType getEncodingType() {
		return VideoEncodingType.RAW;
	}

	// TODO do conversion in C:
	// public BufferedImage decode(int width, int height, int[] inputArray)

	public BufferedImage decode(int width, int height, byte[] rgbImage) {
		assert (rgbImage.length == width * height * 3);

		// inspired by BMPImageReader#read
		// TYPE_INT_RGB is a ARGB pixel format
		BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		WritableRaster raster = bi.getWritableTile(0, 0);

		// buffer for int data
		int argbImage[] = ((DataBufferInt) raster.getDataBuffer()).getData();
		assert (argbImage.length == width * height);
		copyBytesRgb(rgbImage, argbImage);

		// ARGB
		// byte on = (byte) 255;
		// int red = (on << 16);
		// int green = (on << 8);
		// int blue = (on);
		// for (int i = 0; i < 50000; i++) {
		// outputArray[i] = red | green;
		// }

		return bi;
	}

	/**
	 * @see com.fernapp.useragent.decoder.Decoder#shutdown()
	 */
	public void shutdown() {
		// nothing to do
	}

	/**
	 * Copies the bytes from a RGB input array to a ARGB output array.
	 */
	private void copyBytesRgb(byte[] inputRgb, int[] output) {
		// printBits(190);
		// printBits(-66);
		// printBits(0xff);
		// printBits(-66 & 0xff);

		assert (output.length * 3 == inputRgb.length);

		int inputOffset = 0;
		for (int outputPos = 0; outputPos < output.length; outputPos++) {
			// server side uses unsigned bytes. thus, bytes might have negative value in
			// java (a 190 on the server would turn into -66)
			// AND with 0xff repairs this

			int r = (inputRgb[inputOffset + 0] & 0xff) << 16;
			int g = (inputRgb[inputOffset + 1] & 0xff) << 8;
			int b = (inputRgb[inputOffset + 2] & 0xff) << 0;
			output[outputPos] = r | g | b;
			inputOffset += 3;
		}
	}

}
