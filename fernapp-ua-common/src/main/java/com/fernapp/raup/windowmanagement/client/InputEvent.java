package com.fernapp.raup.windowmanagement.client;

import java.io.Serializable;


/**
 * The base class of a user input event that the client can send to the server. The
 * current mouse position should always be piggybacked when available.
 * @author Markus
 */
public class InputEvent implements Serializable {

	private String windowId;
	private Integer pointerPositionTop;
	private Integer pointerPositionLeft;

	// private Integer mouseWheelDelta;

	public InputEvent() {
		// default constructor
	}

	public InputEvent(String windowId) {
		this.windowId = windowId;
	}

	/**
	 * @return the windowId
	 */
	public String getWindowId() {
		return windowId;
	}

	/**
	 * @param windowId the windowId to set
	 */
	public void setWindowId(String windowId) {
		this.windowId = windowId;
	}

	/**
	 * @return the pointerPositionTop
	 */
	public Integer getPointerPositionTop() {
		return pointerPositionTop;
	}

	/**
	 * @param pointerPositionTop the pointerPositionTop to set
	 */
	public void setPointerPositionTop(Integer pointerPositionTop) {
		this.pointerPositionTop = pointerPositionTop;
	}

	/**
	 * @return the pointerPositionLeft
	 */
	public Integer getPointerPositionLeft() {
		return pointerPositionLeft;
	}

	/**
	 * @param pointerPositionLeft the pointerPositionLeft to set
	 */
	public void setPointerPositionLeft(Integer pointerPositionLeft) {
		this.pointerPositionLeft = pointerPositionLeft;
	}

}
