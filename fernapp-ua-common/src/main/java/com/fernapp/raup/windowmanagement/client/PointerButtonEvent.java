package com.fernapp.raup.windowmanagement.client;

/**
 * A mouse click event.
 * @author Markus
 */
public class PointerButtonEvent extends InputEvent {

	public static final int LEFT_BUTTON = 1;
	public static final int RIGHT_BUTTON = 3;

	private boolean buttonPressed;
	private int buttonNumber;

	public PointerButtonEvent() {
		// default constructor
	}

	public PointerButtonEvent(String windowId, boolean buttonPressed, int buttonNumber) {
		super(windowId);
		this.buttonPressed = buttonPressed;
		this.buttonNumber = buttonNumber;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[windowId=" + getWindowId() + ", buttonPressed=" + buttonPressed + ", buttonNumber=" + buttonNumber + "]";
	}

	public boolean isButtonPressed() {
		return buttonPressed;
	}

	public void setButtonPressed(boolean buttonPressed) {
		this.buttonPressed = buttonPressed;
	}

	public int getButtonNumber() {
		return buttonNumber;
	}

	public void setButtonNumber(int buttonNumber) {
		this.buttonNumber = buttonNumber;
	}

}
