package com.fernapp.raup.windowmanagement.client;

import java.io.Serializable;

import com.fernapp.raup.windowmanagement.server.WindowDestroyed;


/**
 * Requests that the window be closed. The request can be ignored by the server. So the
 * corresponding client window should remain opend until a {@link WindowDestroyed} event
 * is received.
 * @author Markus
 */
public class WindowCloseRequest implements Serializable {

	private String windowId;

	public WindowCloseRequest() {
		// default constructor
	}

	public WindowCloseRequest(String windowId) {
		this.windowId = windowId;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[windowId=" + windowId + "]";
	}

	/**
	 * @return the windowId
	 */
	public String getWindowId() {
		return windowId;
	}

	/**
	 * @param windowId the windowId to set
	 */
	public void setWindowId(String windowId) {
		this.windowId = windowId;
	}

}
