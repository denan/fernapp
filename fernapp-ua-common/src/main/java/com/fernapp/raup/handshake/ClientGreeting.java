package com.fernapp.raup.handshake;

import java.io.Serializable;

import com.fernapp.raup.VideoEncodingType;


/**
 * The greeting the client has to send to the server.
 * @author Markus
 */
public class ClientGreeting implements Serializable {

	private String passphrase;
	private int protocolVersion;
	private VideoEncodingType preferedVideoEncoding;

	/**
	 * @return the passphrase
	 */
	public String getPassphrase() {
		return passphrase;
	}

	/**
	 * @param passphrase the passphrase to set
	 */
	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	/**
	 * @return the protocolVersion
	 */
	public int getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @param protocolVersion the protocolVersion to set
	 */
	public void setProtocolVersion(int protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	/**
	 * @return the preferedVideoEncoding
	 */
	public VideoEncodingType getPreferedVideoEncoding() {
		return preferedVideoEncoding;
	}

	/**
	 * @param preferedVideoEncoding the preferedVideoEncoding to set
	 */
	public void setPreferedVideoEncoding(VideoEncodingType preferedVideoEncoding) {
		this.preferedVideoEncoding = preferedVideoEncoding;
	}

}
