package com.fernapp.util;

/**
 * A general purpose call back interface.
 * @author Markus
 */
public interface Callback<T> {

	public void onCallback(T payload) throws Exception;

}
