package com.fernapp.uacommon.middleware.channel;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.Channel;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * Represents the byte level data transport layer (e.g. a socket with an input and output
 * stream). An implementation could add SSL encryption.
 * <p>
 * We do not use {@link SocketChannel} because we don't need non-blocking behavior but
 * want writes to have a timeout.
 * @author Markus
 */
public interface DataChannel extends Channel {

	/**
	 * Something that identifies the remote endpoint (e.g. IP address).
	 */
	String getRemoteEndpointDescriptor();

	InputStream getInputStream();

	OutputStream getOutputStream();

	/**
	 * A thread-safe helper method to write on the output stream. Performs the given write
	 * job and blocks until the write is done or the timeout is reached.
	 */
	void write(WriteJob writeJob, long timeout, TimeUnit unit) throws TimeoutException, IOException;

	public static interface WriteJob {
		void write() throws IOException;
	}

}
