package com.fernapp.uacommon.middleware;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.channel.SocketFactory;


/**
 * A loopback network socket connection.
 * @author Markus
 */
public class LoopbackConnection {

	private static final Logger log = LoggerFactory.getLogger(LoopbackConnection.class);

	private final Socket serverSocket;
	private final Socket clientSocket;

	private final ServerSocket ss;

	public LoopbackConnection() throws Exception {
		log.info("Opening loopback connection");

		// open communication link
		final int port = 1234;
		ss = new ServerSocket(port);
		clientSocket = SocketFactory.createClientSocket();
		Thread clientConnectThread = new Thread(new Runnable() {
			public void run() {
				try {
					clientSocket.connect(new InetSocketAddress("localhost", port), 3000);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		clientConnectThread.start();
		serverSocket = ss.accept();
		clientConnectThread.join();
		SocketFactory.configureServerSocket(serverSocket);

		assert (clientSocket.isConnected());
		assert (serverSocket.isConnected());
	}

	public void close() throws IOException {
		try {
			serverSocket.close();
		} catch (IOException e) {
			// ignore
		}
		try {
			clientSocket.close();
		} catch (IOException e) {
			// ignore
		}
		ss.close();
	}

	/**
	 * @return the serverSocket
	 */
	public Socket getServerSocket() {
		return serverSocket;
	}

	/**
	 * @return the clientSocket
	 */
	public Socket getClientSocket() {
		return clientSocket;
	}

}
