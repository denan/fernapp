package com.fernapp.uacommon.middleware;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.SocketException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.channel.DataChannel;
import com.fernapp.uacommon.middleware.channel.DataChannel.WriteJob;

import flexjson.JSONDeserializer;
import flexjson.JSONException;
import flexjson.JSONSerializer;


/**
 * Messages are exchanged as JSON documents. Everything is UTF-8 encoded. No special
 * delimiter is used to seperate the JSON documents/messages from each other (JSON
 * document are by themselves clearly seperated due to curly brackets).
 * @author Markus
 */
public class JsonMessagingConnection extends AbstractMessagingConnection {

	private static final Logger log = LoggerFactory.getLogger(JsonMessagingConnection.class);

	private final DataChannel dataChannel;
	private final Writer writer;
	private final Reader reader;

	public JsonMessagingConnection(DataChannel dataChannel, String name) {
		super(name);
		this.dataChannel = dataChannel;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(dataChannel.getOutputStream(), "UTF-8"));
			reader = new BufferedReader(new InputStreamReader(dataChannel.getInputStream(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#getDataChannel()
	 */
	@Override
	protected DataChannel getDataChannel() {
		return dataChannel;
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#cleanup()
	 */
	@Override
	protected void cleanup() {
		super.cleanup();
		try {
			reader.close();
		} catch (IOException e) {
			log.error("reader.close()", e);
		}
		try {
			writer.close();
		} catch (IOException e) {
			log.error("writer.close()", e);
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#writeMessage(java.lang.Object)
	 */
	@Override
	protected void writeMessage(Object message) throws IOException {
		final String json = new JSONSerializer().deepSerialize(message);

		if (log.isTraceEnabled()) {
			log.trace("Writing message: " + json);
		}

		try {
			getDataChannel().write(new WriteJob() {
				public void write() throws IOException {
					writer.write(json);
					// writer.write('\n');
					writer.flush();
				}
			}, 5, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			log.warn("Write has timed out. Other side is probably gone.");
			close();
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#readMessage()
	 */
	@Override
	protected Object readMessage() {
		Object obj = null;
		try {
			obj = new JSONDeserializer().deserialize(reader);
		} catch (JSONException e) {
			Throwable cause = e.getCause();
			if (cause instanceof SocketException) {
				log.warn("Receive failed: The connection has been closed");
			} else if ("Stepping back two steps is not supported".equals(e.getMessage())) {
				log.warn("The other side has closed the connection");
			} else if (cause instanceof IOException && "Stream closed".equals(cause.getMessage())) {
				log.warn("The connection has been closed: " + cause.getMessage());
			} else {
				log.error("Receive failed while connection was open", e);
			}
			close();
		}

		return obj;
	}
}
