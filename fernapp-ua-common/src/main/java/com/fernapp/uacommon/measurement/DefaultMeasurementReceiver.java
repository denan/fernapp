package com.fernapp.uacommon.measurement;

import java.util.Arrays;
import java.util.List;


/**
 * @author Markus
 * 
 */
public class DefaultMeasurementReceiver implements MeasurementReceiver {

	private final List<MeasurementProcessor> measurementProcessors;

	public DefaultMeasurementReceiver(MeasurementProcessor... measurementProcessors) {
		this.measurementProcessors = Arrays.asList(measurementProcessors);
	}

	/**
	 * @see com.fernapp.uacommon.measurement.MeasurementReceiver#receiveMeasurement(com.fernapp.uacommon.measurement.DelayCategory,
	 * long, long)
	 */
	public void receiveMeasurement(DelayCategory delayCategory, long delay, long dateSize) {
		Measurement measurement = new Measurement(System.currentTimeMillis(), delayCategory, delay, dateSize);
		receiveMeasurement(measurement);
	}

	/**
	 * @see com.fernapp.uacommon.measurement.MeasurementReceiver#receiveMeasurement(com.fernapp.uacommon.measurement.Measurement)
	 */
	public void receiveMeasurement(Measurement measurement) {
		for (MeasurementProcessor measurementProcessor : measurementProcessors) {
			measurementProcessor.processMeasurement(measurement);
		}
	}

}
