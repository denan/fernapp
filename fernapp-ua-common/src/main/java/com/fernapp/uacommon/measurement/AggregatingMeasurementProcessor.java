package com.fernapp.uacommon.measurement;

import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Aggregating.
 * @author Markus
 * 
 */
public class AggregatingMeasurementProcessor implements MeasurementProcessor {

	private static final Logger log = LoggerFactory.getLogger(AggregatingMeasurementProcessor.class);

	private final int maxMeasurementCount;
	private final Map<DelayCategory, CategoryMeasurements> categoryMeasurements = new ConcurrentHashMap<DelayCategory, CategoryMeasurements>();
	private boolean measurementsAvailable;

	public AggregatingMeasurementProcessor(int maxMeasurementCount) {
		this.maxMeasurementCount = maxMeasurementCount;
		for (DelayCategory delayCategory : DelayCategory.values()) {
			CategoryMeasurements cm = new CategoryMeasurements(delayCategory);
			categoryMeasurements.put(delayCategory, cm);
		}
	}

	/**
	 * @see com.fernapp.uacommon.measurement.MeasurementProcessor#processMeasurement(com.fernapp.uacommon.measurement.Measurement)
	 */
	public void processMeasurement(Measurement measurement) {
		assert (measurement.getDataSize() >= 0);
		assert (measurement.getDelay() >= 0);
		CategoryMeasurements cm = categoryMeasurements.get(measurement.getDelayCategory());
		cm.add(measurement);
		measurementsAvailable = true;
	}

	public MeasurementReport generateReport(DelayCategory delayCategory) {
		CategoryMeasurements cm = categoryMeasurements.get(delayCategory);
		if (cm != null) {
			return cm.generateReport();
		} else {
			return null;
		}
	}

	public void logMeasurements() {
		if (log.isDebugEnabled()) {
			log.debug("Performance measurements:\n" + reportMeasurements());
		}
	}

	public String reportMeasurements() {
		StringBuilder sb = new StringBuilder();
		for (DelayCategory delayCategory : DelayCategory.values()) {
			MeasurementReport mr = generateReport(delayCategory);
			if (mr != null) {
				sb.append(mr.toString());
				sb.append('\n');
			}
		}
		return sb.toString();
	}

	public void reset() {
		for (DelayCategory delayCategory : DelayCategory.values()) {
			CategoryMeasurements cm = categoryMeasurements.get(delayCategory);
			cm.reset();
		}
	}

	private class CategoryMeasurements {
		private DelayCategory delayCategory;
		private Queue<Measurement> latestMeasurements;

		public CategoryMeasurements(DelayCategory delayCategory) {
			this.delayCategory = delayCategory;
			latestMeasurements = new ConcurrentLinkedQueue<Measurement>();

			// fill it with dummy values - otherwise we need synchronization
			for (int i = 0; i < maxMeasurementCount; i++) {
				Measurement m = new Measurement(0, null, 0, 0);
				latestMeasurements.add(m);
			}
		}

		public void add(Measurement measurement) {
			latestMeasurements.add(measurement);
			latestMeasurements.remove();
		}

		public void reset() {
			for (int i = 0; i < maxMeasurementCount; i++) {
				Measurement m = new Measurement(0, null, 0, 0);
				add(m);
			}
		}

		public MeasurementReport generateReport() {
			MeasurementReport report = new MeasurementReport();
			report.delayCategory = delayCategory;
			double delaySum = 0;
			double dataSizeSum = 0;

			for (Measurement m : latestMeasurements) {
				if (m.getDelayCategory() != null) {
					report.count++;
					delaySum += m.getDelay();
					dataSizeSum += m.getDataSize();
				}
			}

			report.avgDelay = delaySum / report.count;
			report.avgDataSize = dataSizeSum / report.count;
			return report;
		}

	}

	public static class MeasurementReport {
		DelayCategory delayCategory;
		int count = 0;
		double avgDelay;
		double avgDataSize;

		public int getCount() {
			return count;
		}

		public double getAvgDelay() {
			return avgDelay;
		}

		public double getAvgDataSize() {
			return avgDataSize;
		}

		/**
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			String out = String.format("Category: %12s,  Avg. Delay: %4d,  Avg. Data Size: %7d,  Count: %2d", delayCategory.toString(),
					(int) getAvgDelay(), (int) getAvgDataSize(), getCount());
			return out;
		}

	}

	public boolean isMeasurementsAvailable() {
		return measurementsAvailable;
	}

}
