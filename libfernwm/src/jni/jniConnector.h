#ifndef JNICONNECTOR_H_
#define JNICONNECTOR_H_

/**
 * Glue code for native to java communication.
 */

#include <jni.h>
#include "../util/common.h"


JNIEnv *getJniEnv();

void initJni(JNIEnv *env, jobject x11ApplicationExecutor);

WindowId parseWindowIdString(JNIEnv *env, jstring windowIdString);

jstring buildWindowIdString(JNIEnv *env, WindowId windowId);

void handlePotentialJniErrors(JNIEnv *env);

jobject encode(JNIEnv *env, jobject instance, jlong windowContentPointer, jlong encContext,
		uint8_t *(*encodeFunction)(WindowContent *wc, DamageReport damageReport, int *dataSize, Bool *reset, void *ctx), int encodingType, jobject damageReport);


jclass class_X11ApplicationExecutor;
jclass class_WindowEventListener;
jclass class_VideoStreamChunk;
jclass class_WindowContent;
jclass class_WindowSettings;
jclass class_ArrayList;
jclass class_DamageReport;

jmethodID method_logDebug;
jmethodID method_receiveNativeMeasurement;
jmethodID method_WindowEventListener_onWindowSettingsChange;
jmethodID method_WindowEventListener_onWindowDestroyed;
jmethodID method_WindowEventListener_onWindowContentUpdate;
jmethodID method_streamChunkClass_ctor;
jmethodID method_windowContent_ctor;
jmethodID method_windowSettings_ctor;
jmethodID method_ArrayList_ctor;
jmethodID method_ArrayList_add;
jmethodID method_DamageReport_ctor;
jmethodID method_DamageReport_getX;
jmethodID method_DamageReport_getY;
jmethodID method_DamageReport_getWidth;
jmethodID method_DamageReport_getHeight;

jobject object_WindowEventListener;
jobject object_X11ApplicationExecutor;

#endif /* JNICONNECTOR_H_ */
