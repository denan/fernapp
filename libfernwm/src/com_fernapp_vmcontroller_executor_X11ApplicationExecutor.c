#include <limits.h>
#include "util/common.h"
#include "jni/jniConnector.h"
#include "util/windowDataManager.h"
#include "x11connector/x11connector.h"
#include "x11connector/x11eventHandler.h"
#include "x11connector/x11input.h"
#include "x11connector/x11capture.h"
#include "com_fernapp_vmcontroller_executor_X11ApplicationExecutor.h"


JNIEXPORT jboolean JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor__1init
  (JNIEnv *env, jobject x11ApplicationExecutor, jstring displayPort) {

	initJni(env, x11ApplicationExecutor);

	if(displayPort != NULL) {
		Bool initSuccess;
		const char *str = (*env)->GetStringUTFChars(env, displayPort, NULL);
		if (str == NULL) {
			logError("GetStringUTFChars failed");
		} else {
			initSuccess = x11ConnectorInit(str);
		}
		(*env)->ReleaseStringUTFChars(env, displayPort, str);
		return initSuccess;
	} else {
		logError("displayPort is null");
		return False;
	}
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor__1eventProcessingLoop
  (JNIEnv *env, jobject instance) {

	x11EventLoop();
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor__1shutdown
  (JNIEnv *env, jobject instance) {

	x11Shutdown();
}


JNIEXPORT jobject JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor__1captureWindowContent
  (JNIEnv *env, jobject instance, jstring wId) {

	WindowId windowId = parseWindowIdString(env,wId);
	WindowContent *windowContent = x11CaptureWindowContent(windowId);
	jobject wcObject = NULL;
	if( windowContent != NULL ) {
		jlong windowContentPointer = (jlong)(intptr_t)windowContent;
		wcObject = (*env)->NewObject(env, class_WindowContent, method_windowContent_ctor, windowContentPointer);
	}
	return wcObject;
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_movePointer
  (JNIEnv *env, jobject instance, jstring windowId, jint x, jint y) {

	movePointer(parseWindowIdString(env,windowId), x, y);
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_pointerButtonAction
  (JNIEnv *env, jobject instance, jstring wId, jint x, jint y, jboolean buttonPressed, jint buttonNumber ) {

	pointerButtonAction(parseWindowIdString(env,wId), x, y, buttonPressed, buttonNumber);
}

JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_keyboardAction
  (JNIEnv *env, jobject instance, jstring wId, jlong keysym, jboolean pressed) {

	keyboardAction(parseWindowIdString(env,wId), keysym, pressed);
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_requestWindowResize
  (JNIEnv *env, jobject instance, jstring wId, jint width, jint height) {

	requestWindowResize(parseWindowIdString(env,wId), width, height);
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor__1requestWindowClose
  (JNIEnv *env, jobject instance, jstring wId) {

	requestWindowClose(parseWindowIdString(env,wId));
}


JNIEXPORT jobject JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_getOpenWindows
  (JNIEnv *env, jobject instance) {

	return getOpenWindows(env);
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_lockWindowData
  (JNIEnv *env, jobject instance) {

	lockWindowData();
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_executor_X11ApplicationExecutor_unlockWindowData
  (JNIEnv *env, jobject instance) {

	unlockWindowData();
}

