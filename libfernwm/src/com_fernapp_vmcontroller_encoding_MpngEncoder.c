#include "util/common.h"
#include "jni/jniConnector.h"
#include "encoders/mpngEncoder.h"
#include "com_fernapp_vmcontroller_encoding_MpngEncoder.h"


JNIEXPORT jlong JNICALL Java_com_fernapp_vmcontroller_encoding_MpngEncoder__1init
  (JNIEnv *env, jobject instance) {

	ASSERT( sizeof(void*) <= sizeof(jlong) );
	void *encoderContext = mpngEncoderInit();
	return (jlong)(intptr_t)encoderContext;
}


JNIEXPORT jobject JNICALL Java_com_fernapp_vmcontroller_encoding_MpngEncoder__1encode
  (JNIEnv *env, jobject instance, jlong windowContentPointer, jobject damageReport, jlong encContext) {

	int encodingType = 2; // VideoEncodingType#Mpng
	return encode(env, instance, windowContentPointer, encContext, mpngEncoderEncode, encodingType, damageReport);
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_encoding_MpngEncoder__1changeQuality
  (JNIEnv *env, jobject instance, jint quality, jlong encContext) {

	// unsupported
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_encoding_MpngEncoder__1shutdown
  (JNIEnv *env, jobject instance, jlong encContext) {

	void *encoderContext = (void*)(intptr_t) encContext;
	mpngEncoderShutdown(encoderContext);
}
