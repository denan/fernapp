#include "util/common.h"
#include "jni/jniConnector.h"
#include "encoders/rawEncoder.h"
#include "com_fernapp_vmcontroller_encoding_RawEncoder.h"


JNIEXPORT jlong JNICALL Java_com_fernapp_vmcontroller_encoding_RawEncoder__1init
  (JNIEnv *env, jobject instance) {

	ASSERT( sizeof(void*) <= sizeof(jlong) );
	void *encoderContext = rawEncoderInit();
	return (jlong)(intptr_t)encoderContext;
}


JNIEXPORT jobject JNICALL Java_com_fernapp_vmcontroller_encoding_RawEncoder__1encode
  (JNIEnv *env, jobject instance, jlong windowContentPointer, jobject damageReport, jlong encContext) {

	int encodingType = 0; // VideoEncodingType#RAW
	return encode(env, instance, windowContentPointer, encContext, rawEncoderEncode, encodingType, damageReport);
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_encoding_RawEncoder__1changeQuality
  (JNIEnv *env, jobject instance, jint quality, jlong encContext) {

	// unsupported
}


JNIEXPORT void JNICALL Java_com_fernapp_vmcontroller_encoding_RawEncoder__1shutdown
  (JNIEnv *env, jobject instance, jlong encContext) {

	void *encoderContext = (void*)(intptr_t) encContext;
	rawEncoderShutdown(encoderContext);
}
