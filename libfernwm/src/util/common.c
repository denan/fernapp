#include <sys/timeb.h>
#include "common.h"


void freeWindowContent(WindowContent *windowContent) {
	(windowContent->referencesHeld)--;
	if( windowContent->referencesHeld == 0 ) {
		free(windowContent->imageDate);
		free(windowContent);
	} else if( windowContent->referencesHeld < 0 ) {
		logError("Negative referencesHeld");
	}
}


unsigned long getCurrentTimeMillis() {
	struct timeb timeStruct;
	ftime(&timeStruct);
	unsigned long millis = ((1000 * timeStruct.time) + timeStruct.millitm);
	return millis;
}


void fatal(char* reason, ...) {
	va_list args;
	va_start(args, reason);
	_logf(LOG_ERROR, reason, args);
	va_end(args);

	// crash the program to get a stack trace by the JVM
	raise(SIGSEGV);
}


/**
 * Makes sure the 64-bit library does not depend on GLIBC-2.14 unnecessarily.
 * http://stackoverflow.com/questions/8823267/linking-against-older-symbol-version-in-a-so-file
 * http://www.trevorpounds.com/blog/?p=103
 */
void *memcpy_wrapper(void *dest, void *src, int n) {
	GLIBC_COMPAT_SYMBOL(memcpy)
    return memcpy(dest, src, n);
}
