#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>
#include "logging.h"


void _logf(int logLevel, char* message, va_list args) {
	int bufferSize = 1000 + strlen(message);
	char *msgBuffer = malloc(bufferSize);

	int outputSize = vsnprintf(msgBuffer, bufferSize, message, args);

	if( outputSize >= bufferSize ) {
		_log(LOG_ERROR, "_logf buffer size was too small");
	}

	_log(logLevel, msgBuffer);
	free(msgBuffer);
}


void logTrace(char* message, ...) {
	// TODO check if we should log trace messages
	va_list args;
	va_start(args,message);
	_logf(LOG_TRACE, message, args);
	va_end(args);
}


void logDebug(char* message, ...) {
	// TODO check if we should log debug messages
	va_list args;
	va_start(args,message);
	_logf(LOG_DEBUG, message, args);
	va_end(args);
}

void logInfo(char* message, ...) {
	va_list args;
	va_start(args,message);
	_logf(LOG_INFO, message, args);
	va_end(args);
}

void logWarn(char* message, ...) {
	va_list args;
	va_start(args,message);
	_logf(LOG_WARN, message, args);
	va_end(args);
}

void logError(char* message, ...) {
	va_list args;
	va_start(args,message);
	_logf(LOG_ERROR, message, args);
	va_end(args);
}

void printf_log(int logLevel, char* message) {
	char *levelStr = "-";
	switch(logLevel) {
	case LOG_TRACE:
		levelStr = "TRACE";
		break;
	case LOG_DEBUG:
		levelStr = "DEBUG";
		break;
	case LOG_INFO:
		levelStr = "INFO";
		break;
	case LOG_WARN:
		levelStr = "WARN";
		break;
	case LOG_ERROR:
		levelStr = "ERROR";
		break;
	}

	/*
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);
	char *timeStr = asctime(local);
	*/

	printf("%5s  %s\n", levelStr, message);
	fflush(stdout);
}
