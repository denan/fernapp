#ifndef X11INPUT_H_
#define X11INPUT_H_

/**
 * Client to server input events.
 */

#include "../util/common.h"


void movePointer(WindowId windowId, int x, int y);

void pointerButtonAction(WindowId windowId, int x, int y, Bool buttonPressed, int buttonNumber);

void keyboardAction(WindowId windowId, unsigned long keysym, Bool pressed);

void requestWindowResize(WindowId windowId, int width, int height);

void requestWindowClose(WindowId windowId);

#endif
