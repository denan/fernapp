#include <X11/extensions/Xcomposite.h>
#include <X11/extensions/Xrender.h>
#include <X11/extensions/Xdamage.h>
#include <util/windowDataManager.h>
#include <x11connector/x11util.h>
#include <x11connector/x11connector.h>


Bool x11ConnectorInit(const char *displayPort) {
	logInfo("x11connector init");

	logInfo("Glib version %i.%i.%i", glib_major_version, glib_minor_version, glib_micro_version);

	if( sizeof(long) == 4 ) {
		logInfo("libfernwm seems to be running in a 32-bit environment");
	} else {
		logInfo("libfernwm seems to be running in a 64-bit environment");
	}

	initX11Util();
	displayName = malloc(10);
	strcpy(displayName, displayPort);
	mainDpy = getDisplay();
	if(mainDpy == NULL) {
		logError("No X server running on port %s", displayName);
		return False;
	}
	Display *dpy = mainDpy;

	// XComposite
	int event_base, error_base;
	if( !XCompositeQueryExtension( dpy, &event_base, &error_base ) ) {
		logError("Composite extension not available");
		return False;
	}

	// XDamage
	if( !XDamageQueryExtension( dpy, &xDamageEventBase, &xDamageErrorBase ) ) {
		logError("Damage extension not available");
		return False;
	}

	// Redirecting all toplevel windows to offscreen pixmaps
	// most people only have one screen
	root = DefaultRootWindow(dpy);
	X_CATCH_N_LOG( XCompositeRedirectSubwindows( dpy, root, CompositeRedirectAutomatic ) );

	// register events
	// SubstructureNotifyMask for map and unmap events
	// SubstructureRedirectMask turns us into a window manager (MapRequest events)
    XSelectInput(dpy, root, SubstructureNotifyMask | SubstructureRedirectMask);

    // property atoms
    Bool createIfNecessary = True; // if no window manager is used, we need to create them
    _NET_WM_WINDOW_TYPE = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE", !createIfNecessary);
    _NET_WM_WINDOW_TYPE_NORMAL = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_NORMAL", !createIfNecessary);
    _NET_WM_WINDOW_TYPE_SPLASH = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_SPLASH", !createIfNecessary);
    _NET_WM_WINDOW_TYPE_DIALOG = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_DIALOG", !createIfNecessary);
    _NET_WM_WINDOW_TYPE_UTILITY = XInternAtom(dpy, "_NET_WM_WINDOW_TYPE_UTILITY", !createIfNecessary);
    _NET_WM_STATE = XInternAtom(dpy, "_NET_WM_STATE", !createIfNecessary);
    _NET_WM_STATE_MODAL = XInternAtom(dpy, "_NET_WM_STATE_MODAL", !createIfNecessary);
    WM_NAME = XInternAtom(dpy, "WM_NAME", !createIfNecessary);
    WM_TRANSIENT_FOR = XInternAtom(dpy, "WM_TRANSIENT_FOR", !createIfNecessary);
    WM_PROTOCOLS = XInternAtom(dpy, "WM_PROTOCOLS", !createIfNecessary);
    WM_DELETE_WINDOW = XInternAtom(dpy, "WM_DELETE_WINDOW", !createIfNecessary);

	// retrieve the dimension of the screen
	Window rootWindow;
	int x_return, y_return;
	unsigned int width_return, height_return, border_width_return, depth_return;
	Status ggSuccess = 0;
	X_CATCH_N_LOG( ggSuccess = XGetGeometry(dpy, root, &rootWindow, &x_return, & y_return, &width_return, &height_return, &border_width_return, &depth_return) );
	ASSERT(ggSuccess && width_return > 100 && height_return > 100);
	screenWidth = width_return;
	screenHeight = height_return;

    initWindowDataManagement();
    applicationProcessingStart = 0;
    x11ConnectorActive = True;

    return True;
}


void x11Shutdown() {
	logDebug("Shutdown requested - disabling X event processing");
	// we use this to protect against a race condition with the clean up code
	lockWindowData();

	x11ConnectorActive = False;

	// push a dummy event into the queue so that the event loop has a chance to stop
	// we don't use a pooled display because we would get a race condition with closeAllDisplays
	Display *dpy = XOpenDisplay(displayName);
	ASSERT(dpy != NULL);
	XClientMessageEvent dummyEvent;
	memset(&dummyEvent, 0, sizeof(XClientMessageEvent));
	dummyEvent.type = ClientMessage;
	dummyEvent.window = interClientCommunicationWindow;
	dummyEvent.format = 32;

	X_CATCH_N_LOG( Bool sendSuccess = XSendEvent(dpy, interClientCommunicationWindow, 0, 0, (XEvent*)&dummyEvent) );
	ASSERT(sendSuccess);
	XCloseDisplay(dpy); // performs XSync internally

	unlockWindowData();
}
