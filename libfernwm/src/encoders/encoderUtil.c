#include <stdio.h>
#include <stdlib.h>
#include "encoderUtil.h"


#define CREATOR "ICH"
#define RGB_COMPONENT_COLOR 255


WindowContent *readWindow(char *filename) {
	int size, width, height;
	uint8_t *data = readImage(&size, &width, &height, filename);
	ASSERT(width * height * 4 == size);
	WindowContent *wc = malloc(sizeof(WindowContent));
	wc->referencesHeld = 1;
	wc->width = width;
	wc->height = height;
	wc->dataSize = size;
	wc->imageDate = data;
	wc->pixelFormat = BGRA_PIXEL_FORMAT;
	return wc;
}


PPMImage *readPPM(const char *filename)
{
         char buff[16];
         PPMImage *img;
         FILE *fp;
         int c, rgb_comp_color;
         //open PPM file for reading
         fp = fopen(filename, "rb");
         if (!fp) {
              fprintf(stderr, "Unable to open file '%s'\n", filename);
              exit(1);
         }

         //read image format
         if (!fgets(buff, sizeof(buff), fp)) {
              perror(filename);
              exit(1);
         }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6') {
         fprintf(stderr, "Invalid image format (must be 'P6')\n");
         exit(1);
    }

    //alloc memory form image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img) {
         fprintf(stderr, "Unable to allocate memory\n");
         exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#') {
    while (getc(fp) != '\n') ;
         c = getc(fp);
    }

    ungetc(c, fp);
    //read image size information
    if (fscanf(fp, "%d %d", &img->width, &img->height) != 2) {
         fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
         exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
         fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
         exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color!= RGB_COMPONENT_COLOR) {
         fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
         exit(1);
    }

    while (fgetc(fp) != '\n') ;
    //memory allocation for pixel data
    img->data = (PPMPixel*)malloc(img->width * img->height * sizeof(PPMPixel));
    // img->data = (PPMPixel*)malloc(3 * img->x * img->y); // since you're allocating 3 times the data for the r,g,b channels.

    if (!img) {
         fprintf(stderr, "Unable to allocate memory\n");
         exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->width, img->height, fp) != img->height) {
         fprintf(stderr, "Error loading image '%s'\n", filename);
         exit(1);
    }

    fclose(fp);
    return img;
}


void writePPM(const char *filename, int width, int height, PPMPixel *data) {
    FILE *fp;
    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) {
         fprintf(stderr, "Unable to open file '%s'\n", filename);
         exit(1);
    }

    //write the header file
    //image format
    fprintf(fp, "P6\n");

    //comments
    fprintf(fp, "# Created by %s\n",CREATOR);

    //image size
    fprintf(fp, "%d %d\n",width,height);

    // rgb component depth
    fprintf(fp, "%d\n",RGB_COMPONENT_COLOR);

    // pixel data
    fwrite(data, 3 * width, height, fp);
    fclose(fp);
}


void writeImage(uint8_t *data, int dataSize, int width, int height, const char *filename) {
    FILE *fp;
    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) {
         fprintf(stderr, "Unable to open file '%s'\n", filename);
         exit(1);
    }

    fprintf(fp, "%d %d\n", width, height);

    fwrite(data, 1, dataSize, fp);
    fclose(fp);
}


uint8_t *readImage(int *dataSize, int *width, int *height, const char *filename) {
    FILE *fp;
    //open file for output
    fp = fopen(filename, "rb");
    if (!fp) {
         fprintf(stderr, "Unable to open file '%s'\n", filename);
         exit(1);
    }

    //read image size information
    if (fscanf(fp, "%d %d\n", width, height) != 2) {
		 fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
		 exit(1);
	}

    int bufferSize = 1*1000*1000;
    uint8_t buffer[bufferSize];
    int r = fread(buffer, 1, bufferSize, fp);
    ASSERT(r<bufferSize);

    fclose(fp);

    uint8_t *data = malloc(r);
    memcpy_wrapper(data, buffer, r);
    *dataSize = r;
    return data;
}


void writeYUV(const char *filename, int width, int height, uint8_t *planes[], int strides[]) {
	FILE *fp;
	fp = fopen(filename, "wb");
	if (!fp) {
		 fprintf(stderr, "Unable to open file '%s'\n", filename);
		 exit(1);
	}
	fprintf(fp, "P6\n");
	fprintf(fp, "%d %d\n",width,height);
	fprintf(fp, "%d\n",RGB_COMPONENT_COLOR);

	int dataSize = width * height * 3;
	uint8_t *rgbData = malloc(dataSize);
	int i=0;

	// YUV420 data format: http://www.mail-archive.com/libav-user@mplayerhq.hu/msg00582.html
	for(int y=0; y<height; y++){
		for (int x=0; x<width; x++){
			uint8_t pixel = planes[0][y * strides[0] + x];
			rgbData[i++] = pixel;
			rgbData[i++] = pixel;
			rgbData[i++] = pixel;
        }
	}
	ASSERT(i==dataSize);

	fwrite(rgbData, 1, dataSize, fp);
	free(rgbData);

	fclose(fp);
}


uint8_t *addFramePadding(int width, int height, int paddingRight, int paddingBottom, uint8_t *orgFrame, int dataSize) {
	int pixelCount = width * height;
	ASSERT(dataSize % pixelCount == 0);
	int pixelLength = dataSize / pixelCount;
	int orgLinesize = width * pixelLength; // in bytes
	int paddedLinesize = (width + paddingRight) * pixelLength; // in bytes

	uint8_t *paddedFrame = malloc((height+paddingBottom) * paddedLinesize);

	for(int i=0; i<height; i++) {
		memcpy_wrapper(paddedFrame+(i*paddedLinesize), orgFrame+(i*orgLinesize), orgLinesize);
		uint8_t *lastPixelOfLine = orgFrame + (i+1)*orgLinesize - pixelLength;

		// set padding on right
		uint8_t *rightPadding = paddedFrame+(i*paddedLinesize)+orgLinesize;
		for(int p_i=0; p_i<paddingRight; p_i++) {
			memcpy_wrapper(rightPadding+(p_i*pixelLength), lastPixelOfLine, 3);
		}
	}

	// duplicate last line
	uint8_t *lastLine = paddedFrame+((height-1)*paddedLinesize);
	for(int i=0; i<paddingBottom; i++) {
		memcpy_wrapper(paddedFrame + ((height+i)*paddedLinesize), lastLine, paddedLinesize);
	}

	return paddedFrame;
}


uint8_t *convertBGRAtoRGB(int width, int height, uint8_t *data, int dataSize) {
	int pixelCount = width * height;
	ASSERT(pixelCount * 4 == dataSize);
	return convertBGRAtoRGBandCrop(width, height, data, 0, 0, width, height);
}


uint8_t *convertBGRAtoRGBandCrop(int orgWidth, int orgHeight, uint8_t *bgraImage, int cropLeft, int cropTop, int cropWidth, int cropHeight) {
	uint8_t* rgbImage = malloc(cropWidth * cropHeight * 3);

	int srcPos, destPos = 0;
	for(int row=cropTop; row<cropTop+cropHeight; row++) {
		// convert line
		srcPos = (row*orgWidth + cropLeft) * 4;
		for(int i=0; i<cropWidth; i++) {
			rgbImage[destPos + 0] = bgraImage[srcPos + 2];
			rgbImage[destPos + 1] = bgraImage[srcPos + 1];
			rgbImage[destPos + 2] = bgraImage[srcPos + 0];
			srcPos += 4;
			destPos += 3;
		}
	}

	return rgbImage;
}


void drawRectangle(uint8_t *rgbImage, int width, int height, int recX, int recY, int recWidth, int recHeight) {
	uint8_t recPixel[3] = {255, 0, 0};

	for(int i=0; i<recWidth; i++) {
		int pos = (width * recY) + recX + i;
		memcpy_wrapper(rgbImage+(pos*3), recPixel, 3);
	}

	for(int i=0; i<recWidth; i++) {
		int pos = (width * (recY+recHeight-1)) + recX + i;
		memcpy_wrapper(rgbImage+(pos*3), recPixel, 3);
	}

	for(int i=0; i<recHeight; i++) {
		int pos = (width * (recY+i)) + recX;
		memcpy_wrapper(rgbImage+(pos*3), recPixel, 3);
	}

	for(int i=0; i<recHeight; i++) {
		int pos = (width * (recY+i)) + recX + recWidth - 1;
		memcpy_wrapper(rgbImage+(pos*3), recPixel, 3);
	}
}


