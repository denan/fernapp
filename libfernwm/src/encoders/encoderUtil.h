#ifndef PPM_H_
#define PPM_H_

#include "../util/common.h"


typedef struct {
     unsigned char red,green,blue;
} PPMPixel;

typedef struct {
     int width, height;
     PPMPixel *data;
} PPMImage;


WindowContent *readWindow(char *filename);


PPMImage *readPPM(const char *filename);
void writePPM(const char *filename, int width, int height, PPMPixel *data);


void writeImage(uint8_t *data, int dataSize, int width, int height, const char *filename);
uint8_t *readImage(int *dataSize, int *width, int *height, const char *filename);


void writeYUV(const char *filename, int width, int height, uint8_t *planes[], int strides[]);


/**
 * Adds padding. For compression it's better to duplicate border pixels than to set them black.
 */
uint8_t *addFramePadding(int width, int height, int paddingRight, int paddingBottom, uint8_t *data, int dataSize);

uint8_t *convertBGRAtoRGB(int width, int height, uint8_t *data, int dataSize);
uint8_t *convertBGRAtoRGBandCrop(int orgWidth, int orgHeight, uint8_t *bgraImage, int cropLeft, int cropTop, int cropWidth, int cropHeight);

void drawRectangle(uint8_t *rgbImage, int width, int height, int recX, int recY, int recWidth, int recHeight);

#endif /* PPM_H_ */
