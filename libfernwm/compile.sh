#!/bin/bash

if [ -z "$JDK_HOME" ]; then
	echo "The JDK_HOME environment variable needs to point to your JDK."
	exit 1
fi

PROJECT_DIR=`pwd`
CC="gcc"
IDIRS="-I $PROJECT_DIR/src -I $JDK_HOME/include -I $JDK_HOME/include/linux -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include"
LIBS="-lglib-2.0 -lgthread-2.0 -lX11 -lXcomposite -lXdamage -lXtst -lpng"
CFLAGS="-g -O0 -ggdb -Wall -std=gnu99 -funsigned-char -fPIC $IDIRS"
# use -O3 for better performance and -O0 for precise debugging
# -Werror is removed until h264 encoder and decoder use same avlib version again

# names of source files without 'src/' prefix and without '.c' prefix
SRCS=`(cd src && find . -name "*.c" | sed s/\\\\.c//)`
TESTS=`(cd test && find . -name "*.c" | sed s/\\\\.c//)`
echo Sources are: $SRCS

echo Clean
rm -rf ./bin64 ./bin32

echo Compiling
set -e
mkdir -p bin64/util bin64/encoders bin64/x11connector bin64/jni
mkdir -p bin32/util bin32/encoders bin32/x11connector bin32/jni

for f in $SRCS; do
	(cd src && \
		$CC -m64 $CFLAGS -c ./$f.c -o ../bin64/$f.o; \
		$CC -m32 $CFLAGS -c ./$f.c -o ../bin32/$f.o \
	)
done

echo Building shared library
$CC -m64 -shared $CFLAGS -o ./bin64/libfernwm.so `find ./bin64 -name *.o` -L$PROJECT_DIR/lib64 $LIBS
$CC -m32 -shared $CFLAGS -o ./bin32/libfernwm.so `find ./bin32 -name *.o` -L$PROJECT_DIR/lib32 $LIBS

echo Executing unit tests
LIB_DIR=
BIN_DIR=
LINUX_ARCH=`uname -i`
if [[ "$LINUX_ARCH" != *"64"* ]]; then
	LIB_DIR=$PROJECT_DIR/lib32
	BIN_DIR=$PROJECT_DIR/bin32
else
	LIB_DIR=$PROJECT_DIR/lib64
	BIN_DIR=$PROJECT_DIR/bin64
fi
echo Using bin dir $BIN_DIR and lib dir $LIB_DIR
set -e; for TEST in $TESTS; do \
	echo Building unit test $TEST; \
	(cd test && $CC $CFLAGS -c ./$TEST.c -o $BIN_DIR/$TEST.o -I ../src); \
	$CC $CFLAGS -o $BIN_DIR/$TEST `find $BIN_DIR -name *.o` -L$LIB_DIR $LIBS; \
	rm $BIN_DIR/$TEST.o; \
	echo Running unit test $TEST; \
	(cd $BIN_DIR && ./$TEST)
done
echo Unit tests completed successfully
